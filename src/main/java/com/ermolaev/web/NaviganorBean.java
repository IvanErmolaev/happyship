package com.ermolaev.web;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean(name="navigator")
@ApplicationScoped
public class NaviganorBean implements Serializable {
	public static final long serialVersionUID = 1L;
	
	public String toIndexPage()
	{
		return "/Index.do?faces-redirect=true";
	}
	
	public String toStepOnePage()
	{
		return "/StepOne.do?faces-redirect=true";
	}
	
	public String toStepTwoPage()
	{
		return "/StepTwo.do?faces-redirect=true";
	}
	
	public String toDashboard()
	{
		return "/Dashboard.do?faces-redirect=true";
	}
}
