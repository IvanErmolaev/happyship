package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.CounterpartyMapper;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Counterparty;
import com.ermolaev.db.model.Present;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="cparty")
@ViewScoped
public class CounterpartyBean implements Serializable {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private List<Counterparty> list;
	private List<Present> presentList;
	private Integer[] selectedList;
	private Integer id;
	private String name;
	private String phones;
	private String email;
	private String website;
	private String city;
	private String streetname;
	private String number;
	private String room;
	private String postalcode;
	
	@PostConstruct
	public void init()
	{
		this.clearValues();
		this.updateList();
		this.updatePresentList();
	}
	
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			list = mapper.getCounterparties(false);
		}
		finally{
			session.close();
		}
	}
	
	public void updatePresentList(Counterparty selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			presentList = mapper.getPresentByCparty(selectedElement.getId(), false);
		}
		finally{
			session.close();
		}		
	}
	
	public void updatePresentList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			presentList = mapper.getRealPresents();
		}
		finally{
			session.close();
		}	
	}
	
	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			mapper.addCounterparty(
					name, 
					phones,
					email, 
					website, 
					city, 
					streetname, 
					number, 
					room, 
					postalcode
			);
			session.commit();
			
			//get last cparty
			Counterparty last = mapper.getLastCounterparty(false);
			
			for(Integer it : selectedList)
			{
				mapper.addPresentToCparty(it, last.getId());
			}
			session.commit();
		}
		finally{
			session.close();
		}	
		
		this.updateList();
		this.clearValues();
		this.showMsg("Контрагент добавлен!");
	}
	
	public void update(Counterparty selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			mapper.updateCounterparty(
					selectedElement.getId(),
					selectedElement.getName(), 
					selectedElement.getPhones(),
					selectedElement.getEmail(), 
					selectedElement.getWebsite(), 
					selectedElement.getCity(), 
					selectedElement.getStreetname(), 
					selectedElement.getNumber(), 
					selectedElement.getRoom(), 
					selectedElement.getPostalcode()
			);
			session.commit();
		}
		finally{
			session.close();
		}		
		this.updateList();
		this.showMsg("Контрагент обновлён!");
	}
	
	public void update()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			mapper.updateCounterparty(
					this.id,
					this.name, 
					this.phones,
					this.email, 
					this.website, 
					this.city, 
					this.streetname, 
					this.number, 
					this.room, 
					this.postalcode
			);
			session.commit();
			
			//add presents to counterparty
			if(selectedList != null)
			{
				mapper.removeAllPresents(this.id);
				for(Integer it : selectedList)
				{
					mapper.addPresentToCparty(it, this.id);
				}
				session.commit();
			}
		}
		finally{
			session.close();
		}	
		this.updateList();
		this.showMsg("Контрагент обновлён!");
	}
	
	public void delete(Counterparty selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			mapper.deleteCounterparty(selectedElement.getId());
			session.commit();
		}
		finally{
			session.close();
		}	
		this.updateList();
		this.showMsg("Контрагент удалён");
	}
	
	public void setValues(Counterparty selectedElement)
	{
		this.id = selectedElement.getId();
		this.name = selectedElement.getName();
		this.phones = selectedElement.getPhones();
		this.email = selectedElement.getEmail();
		this.website = selectedElement.getWebsite();
		this.city = selectedElement.getCity();
		this.streetname = selectedElement.getStreetname();
		this.number = selectedElement.getNumber();
		this.room = selectedElement.getRoom();
		this.postalcode = selectedElement.getPostalcode();
		
		this.updatePresentList();
		this.updatePresentListById();
	}
	
	public void updatePresentListById()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			List<Present> presents = mapper.getAllPresentByCparty(this.id);
			selectedList = new Integer[presents.size()];
			for(int i = 0; i < presents.size(); i++)
				selectedList[i] = presents.get(i).getId();
		}
		finally
		{
			session.close();
		}
	}
	
	public void clearValues()
	{
		this.name = "";
		this.phones = "";
		this.email = "";
		this.website = "";
		this.city = "Ставрополь";
		this.streetname = "";
		this.number = "";
		this.room = "";
		this.postalcode = "";
		this.selectedList = null;
		this.updatePresentList();
	}

	public List<Counterparty> getList() {
		return list;
	}

	public void setList(List<Counterparty> list) {
		this.list = list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhones() {
		return phones;
	}

	public void setPhones(String phones) {
		this.phones = phones;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreetname() {
		return streetname;
	}

	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}

	public List<Present> getPresentList() {
		return presentList;
	}

	public void setPresentList(List<Present> presentList) {
		this.presentList = presentList;
	}

	public Integer[] getSelectedList() {
		return selectedList;
	}

	public void setSelectedList(Integer[] selectedList) {
		this.selectedList = selectedList;
	}
}
