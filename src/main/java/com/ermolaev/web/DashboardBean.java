package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.model.Order;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="dashboard")
@ViewScoped
public class DashboardBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private List<Order> list;
	
	@PostConstruct
	public void init()
	{
		this.updateOrders();
	}
	
	public void updateOrders()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			this.list = mapper.getOrders(true, true);
		}
		finally
		{
			session.close();
		}
		this.showMsg("Статус заказа изменён!");
	}
	
	public void updateOrder(Order order)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			mapper.changeStatus(order.getId(), order.getStatus());
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
	
///////////////////////////////////////////////////////////////////////////////
	//	GETTER & SETTERS
///////////////////////////////////////////////////////////////////////////////
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public List<Order> getList() {
		return list;
	}

	public void setList(List<Order> list) {
		this.list = list;
	}
}
