package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.MessageMapper;
import com.ermolaev.db.model.Message;
import com.ermolaev.db.model.MessageType;
import com.ermolaev.model.sclist.SCMessageList;
import com.ermolaev.model.scunit.SCUMessages;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="scMessages")
@SessionScoped
public class SCMessagesBean extends SCMessageList implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	@PostConstruct
	public void init()
	{
		updateData();
	}
	
	@Override
	public void updateData() {
		super.updateData();
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			List<MessageType> types = mapper.getMessageTypes(true, false);
			
			for(MessageType mtype : types)
			{
				List<Message> messages = mapper.getMessagesByType(mtype.getId(), true);

				if(messages.size() != 0)
				{
					SCUMessages sc = new SCUMessages();
					sc.setId(mtype.getId());
					sc.setName(mtype.getName());
					sc.setObjects(messages);	

					this.units.add(sc);
				}
				
			}
		}
		finally
		{
			session.close();
		}			
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
}
