package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Customer;
import com.ermolaev.db.model.Order;
import com.ermolaev.db.model.Present;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="orderOne")
@ViewScoped
public class OrderOneBean implements Serializable {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	public Connection conn;
	
	private Integer orderId;
	
	private Order object;
	private Customer customer;
	private List<Present> presents;
	private Boolean isProcessable;
	
	@PostConstruct
	public void init()
	{
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		this.orderId = Integer.valueOf(request.getParameter("orderId"));
		this.loadOrder();
	}
	
	public void loadOrder()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			PresentMapper presMapper = session.getMapper(PresentMapper.class);
			
			this.object = mapper.getOrderById(this.orderId);
			this.customer = mapper.getCustomerByOrder(this.object.getId());
			this.presents = presMapper.getPresentsByOrder(object.getId());
			
			if(this.object.getStatus().equals(Order.STATUS_WAITING))
			{
				this.isProcessable = false;
			}
			else
			{
				this.isProcessable = true;
			}
		}
		finally
		{
			session.close();
		}
	}
	
	public String makeProcessing()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			this.object = mapper.getOrderById(this.orderId);
			mapper.changeStatus(this.object.getId(), Order.STATUS_PROCESSING);
			session.commit();
			
			this.isProcessable = true;
		}
		finally
		{
			session.close();
		}
		return "#";
	}

/////////////////////////////////////////////////////////////////////////////////
	//	GETTERS & SETTERS
/////////////////////////////////////////////////////////////////////////////////
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Order getObject() {
		return object;
	}

	public void setObject(Order object) {
		this.object = object;
	}

	public List<Present> getPresents() {
		return presents;
	}

	public void setPresents(List<Present> presents) {
		this.presents = presents;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Boolean getIsProcessable() {
		return isProcessable;
	}

	public void setIsProcessable(Boolean isProcessable) {
		this.isProcessable = isProcessable;
	}
}
