package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.PresentType;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="presentType")
@ViewScoped
public class PresentTypeBean implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	///////////////////////////////////////////////////////////////
	//// BEAN DATA
	private Integer id;
	private String name 		= "";
	private Integer order 		= 1;
	private Boolean available 	= false;
	private Boolean deleted 	= false;
	//LIST
	private List<PresentType> list;
	
	
	@PostConstruct
	public void init()
	{
		this.updateList();
	}

////////////////////////////////////////////////////////////////////////////////////////
/// UPDATE LIST
//////////////////////////////
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			list = mapper.getRealPresentTypes();
		}
		finally
		{
			session.close();
		}		
	}

	
////////////////////////////////////////////////////////////////////////////////////////
/// ADD
//////////////////////////////
	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.addPresentType(name, order, available);
			session.commit();
		}
		finally
		{
			session.close();
		}
		
		//set to default
		this.name = "";
		this.available = true;
		this.deleted = false;
		
		this.updateList();
		this.showMsg("Категория добавлена!");
	}
	
////////////////////////////////////////////////////////////////////////////////////////
/// UPDATE VALUE
//////////////////////////////	
	public void update(PresentType selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.updatePresentType(
					selectedElement.getId(), 
					selectedElement.getName(), 
					selectedElement.getOrder(),
					selectedElement.getAvailable()
			);
			session.commit();
		}
		finally
		{
			session.close();
		}	
		
		//update list
		this.updateList();
		this.showMsg("Категория изменена!");
	}
////////////////////////////////////////////////////////////////////////////////////////
/// DELETE VALUE
//////////////////////////////	
	public void delete(PresentType selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.deletePresentType(selectedElement.getId());
			session.commit();
		}
		finally
		{
			session.close();
		}	
		
		//list update
		this.updateList();
		this.showMsg("Категория удалена!");
	}


	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
	public List<PresentType> getList() {
		return list;
	}
	public void setList(List<PresentType> list) {
		this.list = list;
	}
	public Connection getConn() {
		return conn;
	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}	
	public PresentType getPresentTypeById(Integer id)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		PresentType type = null;
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			type = mapper.getPresentTypesById(id);
		}
		finally
		{
			session.close();
		}
		return type;
	}
}
