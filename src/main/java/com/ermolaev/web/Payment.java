package com.ermolaev.web;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

@ManagedBean(name="payment")
@SessionScoped
public class Payment {

	public void pay()
	{
		RequestContext requestContext = RequestContext.getCurrentInstance();  
		requestContext.execute("javascript: submitform();");
	}
}
