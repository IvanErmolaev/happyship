package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Present;
import com.ermolaev.db.model.PresentType;
import com.ermolaev.model.sclist.SCPresentList;
import com.ermolaev.model.scunit.SCUPresents;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="scPresents")
@SessionScoped
public class SCPresentsBean extends SCPresentList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	@PostConstruct
	public void init()
	{
		this.updateData();
	}
	
	public void clearSelectedData(Integer id)
	{
		for(SCUPresents it : units)
		{
			if(it.getId().compareTo(id) == 0)
			{
				it.unselect();
				break;
			}
		}
	}
	
	public Present getPresentById(Integer id)
	{	
		for(SCUPresents it : units)
		{
			for(Present pr : it.getObjects())
			{
				if(pr.getId().compareTo(id) == 0)
					return pr;
			}
		}
		return null;
	}
	
	public void setSelected(Integer typeId, Integer presentId)
	{
		for(SCUPresents it : units)
		{
			if(it.getId().compareTo(typeId) == 0)
			{
				it.setSelected(presentId);
			}
		}
	}
	
	@Override
	public void updateData()
	{
		super.updateData();
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			List<PresentType> types = mapper.getPresentTypes(true, false);
			
			for(PresentType ptype : types)
			{
				List<Present> presents = mapper.getPresentsByType(ptype.getId(), true, false);

				if(presents.size() != 0)
				{
					SCUPresents sc = new SCUPresents();
					sc.setId(ptype.getId());
					sc.setName(ptype.getName());
					sc.setObjects(presents);	
					this.units.add(sc);
				}
			}
		}
		finally
		{
			session.close();
		}	
	}
//////////////////////////////////////////////////////////////////////
	//	GETTERS & SETTERS
//////////////////////////////////////////////////////////////////////
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
}
