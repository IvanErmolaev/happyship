package com.ermolaev.web;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.common.ComString;
import com.ermolaev.common.Crypto;
import com.ermolaev.db.mapper.AuthMapper;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="profile")
@SessionScoped
public class ProfileBean implements Serializable {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private String firstName;
	private String lastName;
	private String password;
	private Boolean logged = false;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String login()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			String hash = Crypto.getSha512(password + firstName + lastName);

			//get mapper
			AuthMapper mapper = session.getMapper(AuthMapper.class);
			//get assertion
			Integer value = mapper.getAuth(hash);
			if(value > 0)
			{
				//clear digits
				this.firstName = ComString.removeDigits(this.firstName);
				this.lastName = ComString.removeDigits(this.lastName);
				//add record into successful log ops
				mapper.addSession(
						new java.sql.Date(new Date().getTime()), 
						new java.sql.Time(new Date().getTime()), 
						firstName, 
						lastName, 
						this.getIp()
				);
				session.commit();
				
				this.logged = true;
				this.password = "";
				return "/admin/Dashboard.do?faces-redirect=true";
			}
			else
			{
				//add record into failure log ops
				mapper.addFailSession(
						new java.sql.Date(new Date().getTime()), 
						new java.sql.Time(new Date().getTime()), 
						firstName, 
						lastName, 
						this.getIp()
				);	
				session.commit();
			}
		}
		finally
		{
			session.close();
		}
		return "/Login1000.do?faces-redirect=true";
	}
	public String logout()
	{
		this.clear();
		return "/Login1000.do?faces-redirect=true";
	}
	public Boolean getLogged() {
		return logged;
	}
	public void setLogged(Boolean logged) {
		this.logged = logged;
	}
	public Connection getConn() {
		return conn;
	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public void clear()
	{
		this.firstName = "";
		this.lastName = "";
		this.password = "";
		this.logged = false;
	}
	
	public String getIp()
	{
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = request.getRemoteAddr();  
		}
		return ipAddress;
	}
	
	public String clearSession()
	{
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		request.getSession().setAttribute("order", null);
		request.getSession().setAttribute("scMessages", null);
		request.getSession().setAttribute("scPresents", null);
		request.getSession().setAttribute("scPresents", null);
		return "#";
	}
}
