package com.ermolaev.web.db;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.ermolaev.common.App;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.PresentType;

@ManagedBean(name="conn", eager=true)
@ApplicationScoped
public class Connection implements Serializable  {

	/*
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	private SqlSessionFactory sessionFactory;
	
	@PostConstruct
	public void init()
	{
		try(InputStream inputStream = Resources.getResourceAsStream("/dbConfig/postgresql/config/mybatis-config.xml"))
		{
			sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		//set static connection
		App.conn = this;
	}

	public SqlSessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SqlSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
