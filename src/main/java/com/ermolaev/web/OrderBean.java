package com.ermolaev.web;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;
import org.primefaces.context.RequestContext;

import com.ermolaev.common.ComString;
import com.ermolaev.common.ComTime;
import com.ermolaev.common.MyCalendar;
import com.ermolaev.db.mapper.MessageMapper;
import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.mapper.PostcardMapper;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Customer;
import com.ermolaev.db.model.Message;
import com.ermolaev.db.model.Order;
import com.ermolaev.db.model.Postcard;
import com.ermolaev.db.model.Present;
import com.ermolaev.db.model.PresentType;
import com.ermolaev.model.scunit.SCUPresents;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="order")
@SessionScoped
public class OrderBean implements Serializable {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	public Connection conn;
	
	@ManagedProperty("#{scPresents}")
	public SCPresentsBean scPresentBean;
	
	private List<Postcard> postcardList;
	private Postcard postcard;
	
	private List<Message> messageList;
	private Integer messageType;
	
	//time
	private ComTime time;
	
	//selected presents
	private Map<Integer, Integer> showcases = null; 
	private List<Present> selectedPresents;
	//Data for application
	private Float price = 999.0f;
	
	private Integer orderId = -1;
	private String firstName;
	private String lastName;
	private String middleName;
	private String phonenumber = "";
	private String email;
	private String referral;
	
	
	private String message;
	private String recipient;
	private String sender;
	
	private Integer deliveryDate;
	private Integer deliveryTime;
	private String payCase = "AC";
	private String city = "Stavropol";
	private String streetname;
	private String number;
	private String room;
	
	@PostConstruct
	public void init()
	{
		selectedPresents = new ArrayList<Present>();
		this.loadDate();
		this.loadShowcases();
		this.loadMessages();
		this.loadPostcards();
	}

	private void loadDate()
	{
		time = new ComTime(new java.util.Date());
		
		this.deliveryDate = 0;
		for(Integer i = 0; i < time.getTimes().size(); i++)
		{
			Entry<java.sql.Time,Boolean> it = time.getTimes().get(i);
			if(it.getValue() == false)
			{
				this.deliveryTime = i;
				break;
			}
		}
	}
	private void loadShowcases()
	{
		showcases = new HashMap<Integer, Integer>();
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			List<PresentType> types = mapper.getPresentTypes(true, false);
			
			for(PresentType ptype : types)
			{
				showcases.put(ptype.getId(), null);
			}
		}
		finally
		{
			session.close();
		}			
	}
	
	public void loadMessages()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			messageList = mapper.getMessages(true);
		}
		finally
		{
			session.close();
		}
	}
	
	public void updateMessage()
	{
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer messageId = Integer.valueOf(params.get("messageId"));
		for(Message it : messageList)
		{
			if(messageId.compareTo(it.getId()) ==  0)
			{
				this.message = it.getMessage();
				break;
			}
		}
	}
	
	public void updatePostcard()
	{
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer messageId = Integer.valueOf(params.get("postcardId"));
		for(Postcard it : postcardList)
		{
			if(messageId.compareTo(it.getId()) ==  0)
			{
				this.postcard = it;
				break;
			}
		}		
	}
	
	public Float getFinalPrice()
	{
		Float finalPrice = 0.0f;
		Collection<Integer> list = showcases.values();
		for(Integer index : list)
		{
			if(index != null)
				finalPrice += scPresentBean.getPresentById(index).getPrice();			
		}
		return finalPrice;
	}
	
	public Integer getFinalComplexity()
	{
		Integer finalComplexity = 0;
		Collection<Integer> list = showcases.values();
		for(Integer index : list)
		{
			if(index != null)
			{
				Present pes = scPresentBean.getPresentById(index);
				if(finalComplexity.compareTo(pes.getComplexity()) < 0)
					finalComplexity = pes.getComplexity();
			}			
		}
		return finalComplexity;		
	}

	public String acceptStepOne()
	{
		Float finalPrice = this.getFinalPrice();

		if(finalPrice > 400)
		{
			return "/StepTwo.do?faces-redirect=true";
		}
		//send error
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ВНИМАНИЕ!", "Нужно выбрать хотя бы один подарок!"));
		return "";
	}
	
	public String acceptStepTwo()
	{
		this.addUnpaidOrder();
		//this.orderId = 111;
		this.pay();
		return "#";
	}
	
	public void loadPostcards()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			postcardList = mapper.getPostcards(true, false);
			
			postcard = new Postcard();
			postcard.setImage("images/mail-back.png");
		}
		finally
		{
			session.close();
		}
	}
	
	public void addSelectedPresent()
	{
		//get map from bean
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Integer presentId = Integer.valueOf(params.get("present"));
		Integer typeId = Integer.valueOf(params.get("presentType"));
		
		//if object already is selected
		Integer last = showcases.get(typeId);
		if(last != null && last.compareTo(presentId) == 0)
		{
			showcases.put(typeId, null);
			scPresentBean.clearSelectedData(typeId);
			this.updatePrice();
			return;
		}
		
		//clear already selected date from current present part
		scPresentBean.clearSelectedData(typeId);
		//ad visual selected present
		scPresentBean.setSelected(typeId, presentId);
		//add selected present into presents data
		showcases.put(typeId, presentId);
		this.updatePrice();
		

		//MAKE SELECTED PRESENT
		//list of selected presents for show them on the StepTwo page
		selectedPresents = new ArrayList<Present>();
		Collection<Integer> list = showcases.values();
		for(Integer presId : list)
		{
			if(presId != null)
			{
				selectedPresents.add(scPresentBean.getPresentById(presId));
			}		
		}
	}
	
	public void updatePrice()
	{
		price = 999.0f;
		Float finalPrice = 0.0f;

		Collection<Integer> list = showcases.values();

		for(Integer index : list)
		{
			if(index != null)
			{
				finalPrice += scPresentBean.getPresentById(index).getPrice();
			}			
		}
		if(finalPrice > 400.0f)
			price += (finalPrice-400.0f);
	}

	public void updateTimeList()
	{
		java.sql.Date dDate = time.getDays().get(deliveryDate).getKey();
		Calendar c = new GregorianCalendar();
		c.setTime(dDate);
		time.updateTime(c.getTime()); //update time
		
		if(time.getTimes().get(deliveryTime).getValue() == true)
		{
			deliveryTime = time.getLastTime();
		}
	}

	public String getSelectedDate()
	{
		try
		{
			java.sql.Date dDate = time.getDays().get(deliveryDate).getKey();
			java.sql.Time dTime = time.getTimes().get(deliveryTime).getKey();
			
			DateFormat tf = new SimpleDateFormat("HH:mm");
			Calendar c = Calendar.getInstance(); c.clear();
			c.setTime(dDate);
			String deliveryDay = String.valueOf(c.get(Calendar.DATE) + " " + ComString.getMonthNameFor(c.get(Calendar.MONTH)));

			//set time into the calendar
			c.clear();
			c.setTimeInMillis(dTime.getTime());
			
			c.set(Calendar.MINUTE, 0);
			String startTime = tf.format(c.getTime());
			c.add(Calendar.HOUR_OF_DAY, 2);
			String finishTime = tf.format(c.getTime());
			String res = String.format(
					"%s с %s до %s", 
					deliveryDay,
					startTime,
					finishTime
			);	
			return res;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return "ERROR";
		}
	}
	
	public void addSelectedPostcard(Postcard selected)
	{
		this.postcard = selected;
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS
////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Map<Integer, Integer> getShowcases() {
		return showcases;
	}
	
	public void setShowcases(Map<Integer, Integer> showcases) {
		this.showcases = showcases;
	}
	
	public Integer getSeleted(Integer typeId)
	{
		return this.showcases.get(typeId);
	}

	public SCPresentsBean getScPresentBean() {
		return scPresentBean;
	}

	public void setScPresentBean(SCPresentsBean scPresentBean) {
		this.scPresentBean = scPresentBean;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ComTime getTime() {
		return time;
	}

	public void setTime(ComTime time) {
		this.time = time;
	}
	
	public Integer getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Integer deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(Integer deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	
	public String getStreetname() {
		return streetname;
	}

	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getFormatPhonenumber()
	{
		return ComString.getClearPhonenumber(this.phonenumber);
	}
	
	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getPayCase() {
		return payCase;
	}

	public void setPayCase(String payCase) {
		this.payCase = payCase;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<Present> getSelectedPresents() {
		return selectedPresents;
	}

	public void setSelectedPresents(List<Present> selectedPresents) {
		this.selectedPresents = selectedPresents;
	}
	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public List<Postcard> getPostcardList() {
		return postcardList;
	}

	public void setPostcardList(List<Postcard> postcardList) {
		this.postcardList = postcardList;
	}

	public Postcard getPostcard() {
		return postcard;
	}

	public void setPostcard(Postcard postcard) {
		this.postcard = postcard;
	}
	
	public List<Message> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<Message> messageList) {
		this.messageList = messageList;
	}

	public Integer getMessageType() {
		return messageType;
	}

	public void setMessageType(Integer messageType) {
		this.messageType = messageType;
	}

	public void addUnpaidOrder()
	{
		//don't add same order twice
		if(this.orderId.compareTo(-1) == 0)
		{
			SqlSession session = conn.getSessionFactory().openSession();
			try
			{
				OrderMapper mapper = session.getMapper(OrderMapper.class);
				
				java.sql.Date dDate = time.getDays().get(deliveryDate).getKey();
				java.sql.Time dTime = time.getTimes().get(deliveryTime).getKey();
				
				Order unpaidOrder = new Order();
				unpaidOrder.setRecipient(this.recipient);
				unpaidOrder.setSender(this.sender);
				unpaidOrder.setMessage(this.message);
				unpaidOrder.setPostcardId(this.postcard.getId());
				unpaidOrder.setDeliveryDate(dDate);
				unpaidOrder.setStartTime(dTime);
				unpaidOrder.setFinishTime(dTime);
				unpaidOrder.setComplexity(this.getFinalComplexity());
				unpaidOrder.setPrice(this.price);
				unpaidOrder.setCity(this.city);
				unpaidOrder.setStreetname(this.streetname);
				unpaidOrder.setNumber(this.number);
				unpaidOrder.setRoom(this.room);
				unpaidOrder.setPostalcode("");
				unpaidOrder.setStatus(Order.STATUS_UNPAID);
				unpaidOrder.setTdate(MyCalendar.nowSqlDate());
				unpaidOrder.setTtime(MyCalendar.nowSqlTime());
				
				Customer unpainCustomer = new Customer();
				unpainCustomer.setFirstName(this.firstName);
				unpainCustomer.setLastName(this.lastName);
				unpainCustomer.setMiddleName(middleName);
				unpainCustomer.setEmail(this.email);
				unpainCustomer.setPhonenumber(ComString.getClearPhonenumber(this.phonenumber));
				
			
				mapper.addOrder("", unpaidOrder, unpainCustomer);
				session.commit();
				
				//get last order
				this.orderId = mapper.getLastOrderByStatus(Order.STATUS_UNPAID);
				
				Collection<Integer> list = showcases.values();
				for(Integer index : list)
				{
					if(index != null)
					{
						Present p = scPresentBean.getPresentById(index);
						mapper.addSelectedPresent(this.orderId, p.getId());
					}		
				}
				session.commit();
			}
			finally
			{
				session.close();
			}			
		}
	}
	
	@PreDestroy
	public void destroy()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			Order or = mapper.getOrderById(this.orderId);
			if(or.getStatus().equals(Order.STATUS_UNPAID))
			{
				mapper.deleteOrderById(this.orderId);
				session.commit();
			}
		}
		finally
		{
			session.close();
		}
	}
	
	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void pay()
	{
		RequestContext requestContext = RequestContext.getCurrentInstance();  
		requestContext.execute("javascript: submitform();");
	}
}
