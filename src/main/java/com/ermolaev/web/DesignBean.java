package com.ermolaev.web;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.SettingsMapper;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="design")
@SessionScoped
public class DesignBean implements Serializable {
	/*
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	public static final String BG_IMAGE = "bg_img_url";
	public static final String BG_COLOR = "bg_color";
	public static final String BG_REPEAT = "bg_repeat";
	public static final String BG_POS_X = "bg_pos_x";
	public static final String BG_POS_Y = "bg_pos_y";
	public static final String BG_ATTACH = "bg_attach";
	public static final String LOGO_IMAGE = "logo_img_url";
	public static final String STEPONE_HEIGHT = "step_one_height";
	public static final String STEPTWO_HEIGHT = "step_two_height";
	
	private String bgImage = "/images/backg.jpg";
	private String logoImage = "/images/logo.png";
	private String bgColor = "0101DF";
	private String bgRepeat = "no-repeat";
	private String bgAttach = "fixed";
	private Integer bgPosX = 0;
	private Integer bgPosY = 0;
	private Integer stepOneHeight = 2800;
	private Integer stepTwoHeight = 2100;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	@PostConstruct
	public void init()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			SettingsMapper mapper = session.getMapper(SettingsMapper.class);
			Integer count = mapper.getCountParameter(BG_IMAGE);
			if(count == 0)
			{
				mapper.addParameter(BG_IMAGE, bgImage);
				mapper.addParameter(BG_COLOR, bgColor);
				mapper.addParameter(BG_REPEAT, bgRepeat);
				mapper.addParameter(BG_POS_X, bgPosX.toString());
				mapper.addParameter(BG_POS_Y, bgPosY.toString());
				mapper.addParameter(BG_ATTACH, bgAttach);
				mapper.addParameter(LOGO_IMAGE, logoImage);
				mapper.addParameter(STEPONE_HEIGHT, stepOneHeight.toString());
				mapper.addParameter(STEPTWO_HEIGHT, stepTwoHeight.toString());
				session.commit();
			}
			else
			{
				bgImage = mapper.getParameter(BG_IMAGE);
				bgColor = mapper.getParameter(BG_COLOR);
				bgRepeat = mapper.getParameter(BG_REPEAT);
				bgPosX = Integer.valueOf(mapper.getParameter(BG_POS_X));
				bgPosY = Integer.valueOf(mapper.getParameter(BG_POS_Y));
				bgAttach = mapper.getParameter(BG_ATTACH);
				logoImage = mapper.getParameter(LOGO_IMAGE);
				stepOneHeight = Integer.valueOf(mapper.getParameter(STEPONE_HEIGHT));
				stepTwoHeight = Integer.valueOf(mapper.getParameter(STEPTWO_HEIGHT));
			}
		}
		finally
		{
			session.close();
		}
	}
	
	public void updateAll()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			SettingsMapper mapper = session.getMapper(SettingsMapper.class);
			mapper.setParameter(BG_IMAGE, bgImage);
			mapper.setParameter(BG_COLOR, bgColor);
			mapper.setParameter(BG_REPEAT, bgRepeat);
			mapper.setParameter(BG_POS_X, bgPosX.toString());
			mapper.setParameter(BG_POS_Y, bgPosY.toString());
			mapper.setParameter(BG_ATTACH, bgAttach);
			mapper.setParameter(LOGO_IMAGE, logoImage);
			mapper.setParameter(STEPONE_HEIGHT, stepOneHeight.toString());
			mapper.setParameter(STEPTWO_HEIGHT, stepTwoHeight.toString());
			session.commit();
		}
		finally
		{
			session.close();
		}		
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public String getBgImage() {
		return bgImage;
	}

	public void setBgImage(String bgImage) {
		this.bgImage = bgImage;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getBgRepeat() {
		return bgRepeat;
	}

	public void setBgRepeat(String bgRepeat) {
		this.bgRepeat = bgRepeat;
	}

	public String getBgAttach() {
		return bgAttach;
	}

	public void setBgAttach(String bgAttach) {
		this.bgAttach = bgAttach;
	}

	public Integer getBgPosX() {
		return bgPosX;
	}

	public void setBgPosX(Integer bgPosX) {
		this.bgPosX = bgPosX;
	}

	public Integer getBgPosY() {
		return bgPosY;
	}

	public void setBgPosY(Integer bgPosY) {
		this.bgPosY = bgPosY;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public Integer getStepOneHeight() {
		return stepOneHeight;
	}

	public void setStepOneHeight(Integer stepOneHeight) {
		this.stepOneHeight = stepOneHeight;
	}

	public Integer getStepTwoHeight() {
		return stepTwoHeight;
	}

	public void setStepTwoHeight(Integer stepTwoHeight) {
		this.stepTwoHeight = stepTwoHeight;
	}
}
