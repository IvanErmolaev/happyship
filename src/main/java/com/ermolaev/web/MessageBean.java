package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.MessageMapper;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Message;
import com.ermolaev.db.model.MessageType;
import com.ermolaev.db.model.Present;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="message")
@ViewScoped
public class MessageBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private List<Message> list;
	private Integer id;
	private String message;
	private String typeName;
	private Integer typeId;
	private Boolean ispattern;
	private Boolean available;
	private List<MessageType> types;
	
	@PostConstruct
	public void init()
	{
		this.clearValues();
		this.updateList();
		this.updateTypesList();
	}
	
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			list = mapper.getRealMessages();
		}
		finally
		{
			session.close();
		}
	}
	
	public void updateTypesList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			types = mapper.getMessageTypes(true, false);
		}
		finally
		{
			session.close();
		}		
	}

	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.addMessage(message, typeId, ispattern, available);
			session.commit();
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Поздравление добавлено!");
	}
	
	public void update()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.updateMessage(id, message, typeId, ispattern, available);
			session.commit();
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Поздравление изменено!");
	}
	
	public void update(Message selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.updateMessage(
					selectedElement.getId(),
					selectedElement.getMessage(), 
					selectedElement.getTypeId(), 
					selectedElement.getIspattern(), 
					selectedElement.getAvailable()
			);
			session.commit();
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Поздравление изменено!");
	}
	
	public void delete(Message selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.deleteMessage(selectedElement.getId());
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		
		this.updateList();
		this.showMsg("Поздравление удалёно!");
	}
	
	public void setValues(Message value)
	{
		this.id = value.getId();
		this.message = value.getMessage();
		this.typeId= value.getTypeId();
		this.ispattern = value.getIspattern();
		this.available = value.getAvailable();
	}
	
	public void clearValues()
	{
		this.message = "";
		this.typeId = 1;
		this.ispattern = true;
		this.available = false;		
	}
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Boolean getIspattern() {
		return ispattern;
	}

	public void setIspattern(Boolean ispattern) {
		this.ispattern = ispattern;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	public List<Message> getList() {
		return list;
	}

	public void setList(List<Message> list) {
		this.list = list;
	}

	public List<MessageType> getTypes() {
		return types;
	}

	public void setTypes(List<MessageType> types) {
		this.types = types;
	}

	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
}
