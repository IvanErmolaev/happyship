package com.ermolaev.web.model;

import java.util.List;

public class SCUnit<ObjectType> {
	
	protected Integer id;
	protected String name;
	protected List<ObjectType> objects;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ObjectType> getObjects() {
		return objects;
	}
	public void setObjects(List<ObjectType> objects) {
		this.objects = objects;
	}
	
	public void unselect() {}
	public void setSelected(Integer id) {}
	public ObjectType getSelected() { return null; }
}
