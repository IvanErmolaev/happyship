package com.ermolaev.web.model;

import java.util.ArrayList;
import java.util.List;

public class SCList<SCUnitName extends SCUnit<?>> {
	protected List<SCUnitName> units;
	
	public void updateData() {
		units = new ArrayList<SCUnitName>();
	}
	
	public void unselectAll() {}

	public List<SCUnitName> getUnits() {
		return units;
	}

	public void setUnits(List<SCUnitName> units) {
		this.units = units;
	}
}
