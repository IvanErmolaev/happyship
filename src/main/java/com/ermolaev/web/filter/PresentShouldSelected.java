package com.ermolaev.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ermolaev.web.OrderBean;

@WebFilter("/StepTwo.do")
public class PresentShouldSelected implements Filter {
	
	@Override
	public void doFilter(
			ServletRequest req, 
			ServletResponse res,
			FilterChain chain
	) throws IOException, ServletException {
		OrderBean orderBean = (OrderBean)((HttpServletRequest)req).getSession().getAttribute("order");
		if(orderBean == null || orderBean.getFinalPrice() < 400.0f)
		{
            String contextPath = ((HttpServletRequest)req).getContextPath();
            ((HttpServletResponse)res).sendRedirect(contextPath + "/StepOne.do");		
		}
		
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}

