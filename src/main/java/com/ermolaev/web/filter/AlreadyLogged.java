package com.ermolaev.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ermolaev.web.ProfileBean;

@WebFilter("/Login1000.do")
public class AlreadyLogged implements Filter {
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException 
	{
		ProfileBean profileBean = (ProfileBean)((HttpServletRequest)request).getSession().getAttribute("profile");
		if(profileBean != null && profileBean.getLogged() == true)
		{
            String contextPath = ((HttpServletRequest)request).getContextPath();
            ((HttpServletResponse)response).sendRedirect(contextPath + "/admin/Dashboard.do");		
		}
		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {}
}
