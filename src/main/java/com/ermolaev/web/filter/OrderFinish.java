package com.ermolaev.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ermolaev.web.ProfileBean;

@WebFilter("/Ok.do")
public class OrderFinish implements Filter {
	
	@Override
	public void doFilter(
			ServletRequest req, 
			ServletResponse res,
			FilterChain chain
	) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest)req).getSession();
		ProfileBean profileBean = (ProfileBean)session.getAttribute("profile");
		if(profileBean != null)
		{
			//remove
			session.removeAttribute("order");
			session.removeAttribute("profile");
		}
		
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}

