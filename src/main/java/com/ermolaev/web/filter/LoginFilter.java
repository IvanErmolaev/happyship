package com.ermolaev.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ermolaev.web.ProfileBean;

@WebFilter("/admin/*")
public class LoginFilter implements Filter {
	
	@Override
	public void doFilter(
			ServletRequest req, 
			ServletResponse res,
			FilterChain chain
	) throws IOException, ServletException {
		ProfileBean profileBean = (ProfileBean)((HttpServletRequest)req).getSession().getAttribute("profile");
		if(profileBean == null || profileBean.getLogged() == false)
		{
            String contextPath = ((HttpServletRequest)req).getContextPath();
            ((HttpServletResponse)res).sendRedirect(contextPath + "/Login1000.do");		
		}
		
		chain.doFilter(req, res);
	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
