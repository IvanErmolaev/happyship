package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.PostcardMapper;
import com.ermolaev.db.model.Postcard;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="postcard")
@ViewScoped
public class PostcardBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private List<Postcard> list;
	
	private Integer id;
	private String name;
	private String image;
	private Boolean available = false;
	
	@PostConstruct
	public void init()
	{
		this.updateList();
	}
	
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			list = mapper.getPostcards(null, false);
		}
		finally
		{
			session.close();
		}
	}
	
	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			mapper.addPostcard(this.name, this.image, this.available);	
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Открытка добавлена!");
	}
	
	public void update(Postcard selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			mapper.updatePostcard(
					selectedElement.getId(),
					selectedElement.getName(), 
					selectedElement.getImage(), 
					selectedElement.getAvailable()
			);	
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.showMsg("Открытка изменена!");		
	}
	
	public void update()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			mapper.updatePostcard(
					this.id,
					this.name, 
					this.image, 
					this.available
			);	
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Открытка изменена!");		
	}
	
	public void delete(Postcard selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			mapper.deletePostcard(selectedElement.getId());
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Открытка удалена!");
	}
	
	public void setValues(Postcard selectedElement)
	{
		this.id = selectedElement.getId();
		this.name = selectedElement.getName();
		this.image = selectedElement.getImage();
		this.available = selectedElement.getAvailable();
	}
	
	public void clearValues()
	{
		this.id = null;
		this.name = "";
		this.image = "";
		this.available = false;		
	}
	
	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
	
////////////////////////////////////////////////////////////////////////////////////
	//	GETTERS & SETTERS
////////////////////////////////////////////////////////////////////////////////////

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public List<Postcard> getList() {
		return list;
	}

	public void setList(List<Postcard> list) {
		this.list = list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}
}
