package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.MessageMapper;
import com.ermolaev.db.model.MessageType;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="messageType")
@ViewScoped
public class MessageTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{conn}")
	private Connection conn;
	
	private List<MessageType> list;
	private Integer id;
	private String name;
	private String description;
	private Boolean available;
	private Boolean deleted;
	
	@PostConstruct
	public void init()
	{
		this.updateList();
		this.clearValues();
	}
	
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			list = mapper.getRealMessageTypes();
		}
		finally
		{
			session.close();
		}		
	}
	
	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.addMessageType(name, description, available);
			session.commit();
		}
		finally
		{
			session.close();
		}	
		this.updateList();
		this.showMsg("Категория позравления добавлена!");
	}
	public void update()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.updateMessageType(id, name, description, available);
			session.commit();
		}
		finally
		{
			session.close();
		}		
		
		this.updateList();
		this.showMsg("Данные категории изменены!");
	}
	
	public void update(MessageType selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.updateMessageType(
					selectedElement.getId(),
					selectedElement.getName(), 
					selectedElement.getDescription(), 
					selectedElement.getAvailable()
			);
			session.commit();
		}
		finally
		{
			session.close();
		}		
		
		this.showMsg("Данные категории изменены!");
	}
	
	public void delete(MessageType selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			mapper.deleteMessageType(selectedElement.getId());
			session.commit();
		}
		finally
		{
			session.close();
		}		
		
		this.updateList();
		this.showMsg("Категория поздравления удалена!");
	}
	
	public void setValues(MessageType value)
	{
		this.id = value.getId();
		this.name = value.getName();
		this.description = value.getDescription();
		this.available = value.getAvailable();
		this.deleted = value.getDeleted();
	}
	
	public void clearValues()
	{
		this.name = "";
		this.description = "";
		this.available = false;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public List<MessageType> getList() {
		return list;
	}

	public void setList(List<MessageType> list) {
		this.list = list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getAvailable() {
		return available;
	}
	
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
}
