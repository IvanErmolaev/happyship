package com.ermolaev.web.ui.impl;

import java.io.Serializable;

public class Dialog implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/* Display or not */
	private Boolean display = false;

	/* Getters and Setters */
	public Boolean getDisplay() {
		return display;
	}
	public void setDisplay(Boolean display) {
		this.display = display;
	}
	
	/* SHOW and HIDE */
	public void show()
	{
		this.display = true;
	}
	public void hide()
	{
		this.display = false;
	}
}
