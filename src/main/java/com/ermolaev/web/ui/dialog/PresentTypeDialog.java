package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.PresentTypeBean;
import com.ermolaev.web.ui.impl.Dialog;

@ManagedBean(name="presentTypeDialog")
@ViewScoped
public class PresentTypeDialog extends Dialog {
	/*
	 *
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{presentType}")
	private PresentTypeBean presentTypeBean;
	
	public void submitDialog()
	{
		presentTypeBean.add();
		this.hide();
	}

	public PresentTypeBean getPresentTypeBean() {
		return presentTypeBean;
	}

	public void setPresentTypeBean(PresentTypeBean presentTypeBean) {
		this.presentTypeBean = presentTypeBean;
	}
}
