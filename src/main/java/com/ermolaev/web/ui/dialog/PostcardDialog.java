package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.PostcardBean;
import com.ermolaev.web.ui.impl.Dialog;

@ManagedBean(name="postcardDialog")
@ViewScoped
public class PostcardDialog extends Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{postcard}")
	private PostcardBean postcardBean;
	
	
	//submit for the adding data
	public void submitAdd()
	{
		postcardBean.add();
		this.hide();
	}
	
	//submit for the updating data
	public void submitUpdate()
	{
		postcardBean.update();
		this.hide();
	}	

	public PostcardBean getPostcardBean() {
		return postcardBean;
	}

	public void setPostcardBean(PostcardBean postcardBean) {
		this.postcardBean = postcardBean;
	}

	@Override
	public void hide() {
		super.hide();
		//clear all values
		postcardBean.clearValues();
	}
}
