package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.MessageBean;
import com.ermolaev.web.ui.impl.Dialog;

@ManagedBean(name="messageDialog")
@ViewScoped
public class MessageDialog extends Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{message}")
	private MessageBean messageBean;
	
	
	//submit for the adding data
	public void submitAdd()
	{
		messageBean.add();
		this.hide();
	}
	
	//submit for the updating data
	public void submitUpdate()
	{
		messageBean.update();
		this.hide();
	}	
	
	public MessageBean getMessageBean() {
		return messageBean;
	}

	public void setMessageBean(MessageBean messageBean) {
		this.messageBean = messageBean;
	}

	@Override
	public void hide() {
		super.hide();
		//clear all values
		messageBean.clearValues();
	}
	
	
}
