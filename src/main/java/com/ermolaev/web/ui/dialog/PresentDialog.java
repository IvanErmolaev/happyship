package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.PresentBean;
import com.ermolaev.web.ui.impl.Dialog;

@ManagedBean(name="presentDialog")
@ViewScoped
public class PresentDialog extends Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{present}")
	private PresentBean presentBean;


	//submit for the adding data
	public void submitAdd()
	{
		presentBean.add();
		this.hide();
	}
	
	//submit for the updating data
	public void submitUpdate()
	{
		presentBean.update();
		this.hide();
	}

	public PresentBean getPresentBean() {
		return presentBean;
	}

	public void setPresentBean(PresentBean presentBean) {
		this.presentBean = presentBean;
	}
	
	@Override
	public void hide() {
		super.hide();
		//clear all values
		presentBean.clearValues();
	}
}
