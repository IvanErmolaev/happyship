package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.MessageTypeBean;
import com.ermolaev.web.ui.impl.Dialog;;

@ManagedBean(name="messageTypeDialog")
@ViewScoped
public class MessageTypeDialog extends Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{messageType}")
	private MessageTypeBean messageTypeBean;

	//submit for the adding data
	public void submitAdd()
	{
		messageTypeBean.add();
		this.hide();
	}
	
	//submit for the updating data
	public void submitUpdate()
	{
		messageTypeBean.update();
		this.hide();
	}	
	
	public MessageTypeBean getMessageTypeBean() {
		return messageTypeBean;
	}

	public void setMessageTypeBean(MessageTypeBean messageTypeBean) {
		this.messageTypeBean = messageTypeBean;
	}
	
	@Override
	public void hide() {
		super.hide();
		//clear all values
		messageTypeBean.clearValues();
	}
}
