package com.ermolaev.web.ui.dialog;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.ermolaev.web.CounterpartyBean;
import com.ermolaev.web.ui.impl.Dialog;

@ManagedBean(name="cpartyDialog")
@ViewScoped
public class CounterpartyDialog extends Dialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty("#{cparty}")
	private CounterpartyBean counterpartyBean;

	
	public void submitAdd()
	{
		counterpartyBean.add();
		this.hide();
	}
	
	public void submitUpdate()
	{
		counterpartyBean.update();
		this.hide();
	}
	
	public CounterpartyBean getCounterpartyBean() {
		return counterpartyBean;
	}

	public void setCounterpartyBean(CounterpartyBean counterpartyBean) {
		this.counterpartyBean = counterpartyBean;
	}
	
	@Override
	public void hide() {
		super.hide();
		//clear all values
		counterpartyBean.clearValues();
	}
}
