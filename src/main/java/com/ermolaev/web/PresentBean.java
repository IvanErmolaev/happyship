package com.ermolaev.web;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Present;
import com.ermolaev.db.model.PresentType;
import com.ermolaev.web.db.Connection;

@ManagedBean(name="present")
@ViewScoped
public class PresentBean implements Serializable {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{conn}")
	private Connection conn;
	
	///////////////////////////////////////////////////////////////
	//// BEAN DATA	
	private Integer id;
	private String name;
	private String description = "";
	private String imageurl;
	private List<PresentType> types;
	private Integer typeId;
	private Integer complexity;
	private Integer amount = 1;
	private Float price = 100.0f;
	private Boolean available = false;
	//list
	private List<Present> list;
	
	@PostConstruct
	public void init()
	{
		this.updateList();
		this.updateTypesList();
	}
	
	public void updateList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			list = mapper.getRealPresents();
		}
		finally
		{
			session.close();
		}			
	}
	
	public void updateTypesList()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			types = mapper.getPresentTypes(true, false);
		}
		finally
		{
			session.close();
		}
	}

	public void add()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.addPresent(name, description, imageurl, typeId, complexity, amount, price, available);	
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Подарок добавлен!");
	}
	
	public void update(Present selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.updatePresent(
					selectedElement.getId(), 
					selectedElement.getName(), 
					selectedElement.getDescription(), 
					selectedElement.getImageurl(), 
					selectedElement.getTypeId(), 
					selectedElement.getComplexity(), 
					selectedElement.getAmount(), 
					selectedElement.getPrice(), 
					selectedElement.getAvailable());
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.showMsg("Данные подарка изменены!");
	}
	
	public void update()
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.updatePresent(id, name, description, imageurl, typeId, complexity, amount, price, available);
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Данные подарка изменены!");
	}
	
	public void delete(Present selectedElement)
	{
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			mapper.deletePresent(selectedElement.getId());
			session.commit(); //send date to DB
		}
		finally
		{
			session.close();
		}
		this.updateList();
		this.showMsg("Подарок удалён!");
	}	
	
	public void setValues(Present selectedElement)
	{
		this.id = selectedElement.getId();
		this.name= selectedElement.getName();
		this.description = selectedElement.getDescription();
		this.imageurl = selectedElement.getImageurl();
		this.typeId = selectedElement.getTypeId();
		this.amount = selectedElement.getAmount();
		this.price = selectedElement.getPrice();
		this.complexity = selectedElement.getComplexity();
	}
	
	public void clearValues()
	{
		this.id = 0;
		this.name= "";
		this.description = "";
		this.imageurl = "";
		this.typeId = 1;
		this.amount = 1;
		this.price = 100.0f;
		this.complexity = 1;
		this.available = false;
	}
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public List<Present> getList() {
		return list;
	}

	public void setList(List<Present> list) {
		this.list = list;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getComplexity() {
		return complexity;
	}

	public void setComplexity(Integer complexity) {
		this.complexity = complexity;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	
	public List<PresentType> getTypes() {
		return types;
	}

	public void setTypes(List<PresentType> types) {
		this.types = types;
	}
	
	public void showMsg(String msg)
	{
		FacesContext.getCurrentInstance()
		.addMessage("msg", 
				new FacesMessage(msg,  " ")
		);
	}
}
