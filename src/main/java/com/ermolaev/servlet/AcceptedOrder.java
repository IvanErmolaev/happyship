package com.ermolaev.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.common.App;
import com.ermolaev.common.YandexAviso;
import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.model.Order;

public class AcceptedOrder extends HttpServlet {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		System.out.println("___AVISO____");
		YandexAviso avisoData = new YandexAviso(req);
		System.out.println("OrderNUMBER: " + avisoData.getOrderNumber());
		System.out.println("Sum: " + avisoData.getOrderSumAmount());
		
		//create local MD5
		String localMD5 = avisoData.getMD5();
		String code = "";
		
		if(localMD5.equalsIgnoreCase(avisoData.getRemoteMD5()))
		{
			code = "0";
			//update database
			this.updateDatabase(Integer.valueOf(avisoData.getOrderNumber()));
		}
		else
		{
			code = "1";
		}
		
		
		resp.setContentType("application/xml");
		PrintWriter out = resp.getWriter();
		
		String result = avisoData.getPostRequest(code);
		
		System.out.println(result);
		out.print(result);
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void updateDatabase(Integer orderId)
	{
		SqlSession session = App.conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			mapper.changeStatus(orderId, Order.STATUS_WAITING);
			session.commit();
		}
		finally
		{
			session.close();
		}		
	}
}