package com.ermolaev.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.SqlSession;

import com.ermolaev.common.App;
import com.ermolaev.common.YandexCheck;
import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.model.Order;

public class CheckOrder extends HttpServlet {
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("___CHECK_ORDER____");
		YandexCheck checkData = new YandexCheck(req);
		System.out.println("OrderNUMBER: " + checkData.getOrderNumber());
		System.out.println("Sum: " + checkData.getOrderSumAmount());
		
		//create local MD5
		String localMD5 = checkData.getMD5();
		String code = "";
		
		if(localMD5.equalsIgnoreCase(checkData.getRemoteMD5()))
		{
			code = "0";
		}
		else
		{
			code = "1";
		}
		
		//test order from database
		if(this.databaseTest(
				Integer.valueOf(checkData.getOrderNumber()), 
				Float.valueOf(checkData.getOrderSumAmount())
				) 
				== false)
		{
			code = "100";
		}
		
		resp.setContentType("application/xml");
		PrintWriter out = resp.getWriter();
		String result = checkData.getPostRequest(code);
		
		System.out.println(result);
		out.print(result);
		
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean databaseTest(Integer orderId, Float orderSumAmount)
	{
		SqlSession session = App.conn.getSessionFactory().openSession();
		try
		{
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			Order order = mapper.getOrderById(orderId);
			
			boolean result = true;
			
			if(order == null) {
				return false;
			}
			if(order.getPrice().compareTo(orderSumAmount) != 0) {
				result = false;
			}
			if(order.getStatus().compareTo(Order.STATUS_UNPAID) != 0) {
				result = false;
			}
			return result;
		}
		finally
		{
			session.close();
		}
	}
}