package com.ermolaev.model.scunit;

import com.ermolaev.db.model.Present;
import com.ermolaev.web.model.SCUnit;

public class SCUPresents extends SCUnit<Present> {
	
	@Override
	public void setSelected(Integer id) {
		for(int i = 0; i < objects.size(); i++)
		{
			if(objects.get(i).getId().compareTo(id) == 0)
			{
				objects.get(i).select();
				break;
			}
		}
	}
	
	@Override
	public void unselect() {
		super.unselect();
		for(int i = 0; i < objects.size(); i++)
		{
			objects.get(i).unselect();
		}
	}
}
