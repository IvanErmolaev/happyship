package com.ermolaev.model.sclist;

import com.ermolaev.model.scunit.SCUPresents;
import com.ermolaev.web.model.SCList;

public class SCPresentList extends SCList<SCUPresents> {
	
	@Override
	public void unselectAll() {
		for(SCUPresents it : units)
			it.unselect();
	}
}
