package com.ermolaev.common;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MyCalendar extends GregorianCalendar {

//////////////////////////////////////////////////////////////////////////////
	//	STATIC FUNCTIONS
//////////////////////////////////////////////////////////////////////////////	
	public static java.sql.Date makeSqlDate(int year, int month, int dayOfMonth)
	{
		return new MyCalendar(year, month, dayOfMonth).getSqlDate();
	}
	
	public static java.sql.Time makeSqlTime(int hourOfDay, int minute, int second)
	{
		return new MyCalendar(2015, 1, 1, hourOfDay,minute,second).getSqlTime();
	}
	
	public static java.util.Date makeTimeByDate(java.util.Date date, int hourOfDay, int minute, int second)
	{
		MyCalendar cal = new MyCalendar();
		cal.setTime(date);
		cal.setTime(hourOfDay, minute, second);
		return cal.getTime();
	}
	
	public static java.sql.Date nowSqlDate()
	{
		return new MyCalendar().getSqlDate();
	}
	
	public static java.sql.Time nowSqlTime()
	{
		return new MyCalendar().getSqlTime();
	}

//////////////////////////////////////////////////////////////////////////////
	//	CONSTRUCTORS
//////////////////////////////////////////////////////////////////////////////	
	public MyCalendar()
	{
		super();
		//set current time
		this.setTime(new java.util.Date());
	}

	public MyCalendar(int year, int month, int dayOfMonth)
	{
		super(year, month-1, dayOfMonth);
	}
	
	public MyCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute)
	{
		super(year, month-1, dayOfMonth, hourOfDay, minute);
	}
		
	public MyCalendar(int year, int month, int dayOfMonth, int hourOfDay, int minute, int second)
	{
		super(year, month-1, dayOfMonth, hourOfDay, minute, second);
	}
	
	public void setDate(int year, int month, int dayOfMonth)
	{
		this.set(Calendar.YEAR, year);
		this.set(Calendar.MONTH, month-1);
		this.set(Calendar.DATE, dayOfMonth);
	}
	
	public void setTime(int hourOfDay, int minute, int second)
	{
		this.set(Calendar.HOUR_OF_DAY, hourOfDay);
		this.set(Calendar.MINUTE, minute);
		this.set(Calendar.SECOND, second);	
	}
	
////////////////////////////////////////////////////////////////////////////////////
	// SET DATE
////////////////////////////////////////////////////////////////////////////////////
	public void setYear(int year)
	{
		this.set(Calendar.YEAR, year);
	}
	
	public void setMonth(int month)
	{
		this.set(Calendar.MONTH, month-1);
	}
	
	public void setDayOfMonth(int dayOfMonth)
	{
		this.set(Calendar.DAY_OF_MONTH, dayOfMonth);
	}
	
////////////////////////////////////////////////////////////////////////////////////
	// SET TIME
////////////////////////////////////////////////////////////////////////////////////	
	public void setHours(int hourOfDay)
	{
		this.set(Calendar.HOUR_OF_DAY, hourOfDay);
	}
	
	public void setMinute(int minute)
	{
		this.set(Calendar.MINUTE, minute);
	}
	
	public void setSecond(int second)
	{
		this.set(Calendar.SECOND, second);
	}
	
////////////////////////////////////////////////////////////////////////////////////
	// GET SQL DATE
////////////////////////////////////////////////////////////////////////////////////	
	public java.sql.Date getSqlDate()
	{
		return new java.sql.Date(this.getTimeInMillis());
	}
////////////////////////////////////////////////////////////////////////////////////
	// GET SQL TIME
////////////////////////////////////////////////////////////////////////////////////	
	public java.sql.Time getSqlTime()
	{
		return new java.sql.Time(this.getTimeInMillis());
	}
	
	public void addDate(int amountDays)
	{
		this.add(Calendar.DATE, amountDays);
	}
	
	public void addHours(int amountHours)
	{
		this.add(Calendar.HOUR, amountHours);
	}
	
	public void addMinutes(int amountMinutes)
	{
		this.add(Calendar.MINUTE, amountMinutes);
	}
}
