package com.ermolaev.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class Crypto {

	static
	{
		Security.addProvider(new BouncyCastleProvider());
	}
	
	public static String getSha512(String value)
	{
		MessageDigest md;
		try {
			
			md = MessageDigest.getInstance("SHA-512", "BC");
			byte[] digest = md.digest(value.getBytes());
			return new String(Hex.encodeHex(digest));
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
	
	public static String getMD5(String value)
	{
		MessageDigest md;
		try {
			
			md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(value.getBytes());
			return new String(Hex.encodeHex(digest));
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
}
