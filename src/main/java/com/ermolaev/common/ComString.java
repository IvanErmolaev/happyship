package com.ermolaev.common;

public class ComString {
	public static String clearHTML(String value)
	{
		String pattern = "</{0,1}\\s*"
				 	   + "(((\\w+\\s*)={0,1}" //attribute
				 	   + "(\"|'){0,1}((\\w+\\s*-*_*)*" //parameter
				 	   + "(:|;|\\)|\\()*"
				 	   + "/{0,2}(\\w*\\s*/{0,1}\\.{0,1}&*;*\\?*=*-*\\+*%*)(\"|'){0,1}))\\s*)*" //value
				 	   + "/{0,1}>";
		return value.replaceAll(pattern, "");
	}
	
	public static String getSiteNameFromURL(String value)
	{
		String pattern1 = "(http://)*(https://)*";
		String pattern2 = "/{1,1}(\\w+-*_*+\\+*%*&*\\.*\\?*=*)*";
		return value.replaceAll(pattern1, "").replaceAll(pattern2, "");
	}
	
	public static String removeDigits(String value)
	{
		String pattern = "([0-9])*";
		return value.replaceAll(pattern, "");
	}
	
	public static String getMonthName(Integer value)
	{
		switch(value)
		{
			case 0 : return "Январь";
			case 1 : return "Февраль";
			case 2 : return "Март";
			case 3 : return "Апрель";
			case 4 : return "Май";
			case 5 : return "Июнь";
			case 6 : return "Июль";
			case 7 : return "Август";
			case 8 : return "Сентябрь";
			case 9 : return "Октябрь";
			case 10 : return "Ноябрь";
			case 11 : return "Декабрь";
			default : return "#Ошибка#";
		}
	}
	
	public static String getMonthNameFor(Integer value)
	{
		switch(value)
		{
			case 0 : return "Января";
			case 1 : return "Февраля";
			case 2 : return "Марта";
			case 3 : return "Апреля";
			case 4 : return "Мая";
			case 5 : return "Июня";
			case 6 : return "Июля";
			case 7 : return "Августа";
			case 8 : return "Сентября";
			case 9 : return "Октября";
			case 10 : return "Ноября";
			case 11 : return "Декабря";
			default : return "#Ошибка#";
		}
	}
	
	public static String getClearPhonenumber(String value)
	{
		String pattern = "(\\+[0-9]?)?(\\s?\\(?)?(\\)?\\s?)?(\\-?)?";
		return value.replaceAll(pattern, "");
	}
}
