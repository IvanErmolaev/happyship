package com.ermolaev.common;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;


public class ComHTTP {
	private final static String USER_AGENT = "Mozilla/5.0";
	
	public static void sendPost(String requestURL, Map<String, String> params) throws IOException
	{
		String url = "https://" + requestURL;
		
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
 
		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 

		String urlParameters = "";
		//create urlString
		Set<String> keys = params.keySet();
		Iterator<String> it = keys.iterator();
		String k = null;
		while((k = it.next()) != null)
		{
			urlParameters += k + "=" + params.get(k);
			if(it.hasNext() == true) {
				urlParameters += "&";
			}
		}
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
 
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
 
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
	}
}
