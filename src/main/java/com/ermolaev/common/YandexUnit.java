package com.ermolaev.common;

import javax.servlet.http.HttpServletRequest;

public class YandexUnit {
	private String action;
	private String remoteMD5;
	private String orderSumAmount;
	private String orderSumCurrencyPaycash;
	private String orderSumBankPaycash;
	private String shopId;
	private String invoiceId;
	private String customerNumber;
	private String shopPassword;
	private String orderNumber;
	
	public YandexUnit(HttpServletRequest req)
	{
		this.action = req.getParameter("action");
		this.remoteMD5 = req.getParameter("md5");
		this.orderSumAmount = req.getParameter("orderSumAmount");
		this.orderSumCurrencyPaycash = req.getParameter("orderSumCurrencyPaycash");
		this.orderSumBankPaycash = req.getParameter("orderSumBankPaycash");
		this.shopId = "47293";
		this.invoiceId = req.getParameter("invoiceId");
		this.customerNumber = req.getParameter("customerNumber");
		this.shopPassword = "b3vv8MR77PYderKTZrCd";
		this.orderNumber = req.getParameter("orderNumber");
	}
	
	public String getMD5()
	{
		String sumString = "";
		sumString += this.action + ";";
		sumString += this.orderSumAmount + ";";
		sumString += this.orderSumCurrencyPaycash + ";";
		sumString += this.orderSumBankPaycash + ";";
		sumString += this.shopId + ";";
		sumString += this.invoiceId + ";";
		sumString += this.customerNumber + ";";
		sumString += this.shopPassword;	
		return Crypto.getMD5(sumString);
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getRemoteMD5() {
		return remoteMD5;
	}

	public void setRemoteMD5(String remoteMD5) {
		this.remoteMD5 = remoteMD5;
	}

	public String getOrderSumAmount() {
		return orderSumAmount;
	}

	public void setOrderSumAmount(String orderSumAmount) {
		this.orderSumAmount = orderSumAmount;
	}

	public String getOrderSumCurrencyPaycash() {
		return orderSumCurrencyPaycash;
	}

	public void setOrderSumCurrencyPaycash(String orderSumCurrencyPaycash) {
		this.orderSumCurrencyPaycash = orderSumCurrencyPaycash;
	}

	public String getOrderSumBankPaycash() {
		return orderSumBankPaycash;
	}

	public void setOrderSumBankPaycash(String orderSumBankPaycash) {
		this.orderSumBankPaycash = orderSumBankPaycash;
	}

	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getShopPassword() {
		return shopPassword;
	}

	public void setShopPassword(String shopPassword) {
		this.shopPassword = shopPassword;
	}
}
