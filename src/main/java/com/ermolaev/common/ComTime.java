package com.ermolaev.common;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map.Entry;



public class ComTime {
	
	//today
	private Date now;
	private List<Entry<java.sql.Date, String>> days;
	private List<Entry<java.sql.Time, Boolean>> times;
	

	public ComTime() {}
	
	public ComTime(Date now)
	{
		this.now = now;
		this.count();
	}
	
	public void count()
	{
		days = new ArrayList<Entry<java.sql.Date, String>>();
		times = new ArrayList<Entry<java.sql.Time, Boolean>>();
		this.createTime();
		this.createDate();		
	}

	private void createTime()
	{
		times.clear();
		MyCalendar c = new MyCalendar();
		c.setTime(now);
		
		MyCalendar nowCal = new MyCalendar();
		nowCal.setTime(now);
		
		if(
			now.after(MyCalendar.makeTimeByDate(now, 20, 10, 0)) == true &&
			now.before(MyCalendar.makeTimeByDate(now, 23, 59, 59))
		)
		{
			//next day
			c.addDate(1);
		}
		
		for(int i = 8; i <= 20; i += 2)
		{
			c.setTime(i, 10, 0);
			if(c.getSqlTime().compareTo(nowCal.getTime()) > 0)
			{
				times.add(new SimpleEntry<java.sql.Time, Boolean>(c.getSqlTime(), false));
			}
			else
			{
				times.add(new SimpleEntry<java.sql.Time, Boolean>(c.getSqlTime(), true));
			}
		}
	}
	
	public void updateTime(Date selected)
	{
		//if day isn't equal today
		if(selected.equals(now) == false)
		{
			this.allTimesInFalse();
		}
		else
		{
			this.createTime();
		}
	}

	private void createDate()
	{
		String firstDayName = "Сегодня";
		MyCalendar cal = new MyCalendar();
		cal.setTime(now);
		
		MyCalendar cal2 = new MyCalendar();
		cal2.setTime(now);
		
		if(
			now.after(MyCalendar.makeTimeByDate(now, 20, 10, 0)) == true &&
			now.before(MyCalendar.makeTimeByDate(now, 23, 59, 59))
		)
		{
			//next day
			cal.addDate(1);
			firstDayName = "Завтра";
		}

		for(int i = 0; i < 7; i++)
		{
			String name = cal.get(Calendar.DAY_OF_MONTH) + " " + ComString.getMonthNameFor(cal.get(Calendar.MONTH));
			Entry<java.sql.Date, String> value 
				= new SimpleEntry<java.sql.Date, String>(
								cal.getSqlDate(), 
								name
			);
			cal.addDate(1);
			days.add(value);			
		}
		
		days.get(0).setValue(firstDayName);
	}
	
	public Integer getLastTime()
	{
		for(Integer i = 0; i < times.size(); i++)
		{
			Boolean it = times.get(i).getValue();
			if(it == false) return i;
		}
		return -1;
	}
	
	public void allTimesInTrue()
	{
		for(int i = 0; i < 7; i++)
			times.get(i).setValue(true);
	}
	public void allTimesInFalse()
	{
		for(int i = 0; i < 7; i++)
			times.get(i).setValue(false);
	}

	public List<Entry<java.sql.Time, Boolean>> getTimes() {
		return times;
	}

	public void setTimes(List<Entry<java.sql.Time, Boolean>> times) {
		this.times = times;
	}

	public List<Entry<java.sql.Date, String>> getDays() {
		return days;
	}

	public void setDays(List<Entry<java.sql.Date, String>> days) {
		this.days = days;
	}

	public void setNow(Date now) {
		this.now = now;
	}
}
