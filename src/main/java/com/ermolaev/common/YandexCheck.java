package com.ermolaev.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class YandexCheck extends YandexUnit{
	
	public YandexCheck(HttpServletRequest req)
	{
		super(req);
	}
	
	public String getPostRequest(String code)
	{
		Date today = new Date();
		SimpleDateFormat conv = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		result += String.format(
				"<checkOrderResponse performedDatetime=\"%s\" code=\"%s\" invoiceId=\"%s\" shopId=\"%s\"/>",
				conv.format(today),
				code,
				this.getInvoiceId(),
				this.getShopId()	
		);
		
		return result;
	}
}
