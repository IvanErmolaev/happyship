package com.ermolaev.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class YandexAviso extends YandexUnit {

	private String paymentDatetime;
	private String cps_user_country_code;
	
	public YandexAviso(HttpServletRequest req)
	{
		super(req);
		this.paymentDatetime = req.getParameter("paymentDatetime");
		this.cps_user_country_code = req.getParameter("cps_user_country_code");
	}

	public String getPaymentDatetime() {
		return paymentDatetime;
	}

	public void setPaymentDatetime(String paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getCps_user_country_code() {
		return cps_user_country_code;
	}

	public void setCps_user_country_code(String cps_user_country_code) {
		this.cps_user_country_code = cps_user_country_code;
	}
	
	public String getPostRequest(String code)
	{
		Date today = new Date();
		SimpleDateFormat conv = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		result += String.format(
				"<paymentAvisoResponse performedDatetime =\"%s\" code=\"%s\" invoiceId=\"%s\" shopId=\"%s\"/>",
				conv.format(today),
				"0",
				this.getInvoiceId(),
				this.getShopId()
		);
		
		return result;
	}
}
