package com.ermolaev.db.model;

/**
 * @author ermolaev
 *
 */
public class Present {

	private Integer id;
	private String name;
	private String description;
	private String imageurl;
	private Integer typeId;
	private String typeName;
	private Integer complexity;
	private Float price;
	private Integer amount;
	private Boolean available;
	private Boolean deleted;
	
	private Boolean selected = false;
	private String styleClassName = "noSelected";
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Integer getComplexity() {
		return complexity;
	}
	public void setComplexity(Integer complexity) {
		this.complexity = complexity;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Boolean getSelected() {
		return selected;
	}

	public void select() {
		this.selected = true;
		this.styleClassName = "selected";
	}
	
	public void unselect() {
		this.selected = false;
		this.styleClassName = "noSelected";
	}
	
	public void setSelected(Boolean selected) {
		this.selected = true;
	}
	
	public String getStyleClassName() {
		return styleClassName;
	}
	public void setStyleClassName(String styleClassName) {
		this.styleClassName = styleClassName;
	}
}
