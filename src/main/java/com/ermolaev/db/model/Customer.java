package com.ermolaev.db.model;

public class Customer {
	private Integer id;
	private String lastName;
	private String firstName;
	private String middleName;
	private String phonenumber;
	private String email;
	private String referral;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getReferral() {
		return referral;
	}
	public void setReferral(String referral) {
		this.referral = referral;
	}
	
	public Customer copy()
	{
		Customer cs = new Customer();
		cs.lastName = lastName;
		cs.firstName = firstName;
		cs.middleName = middleName;
		cs.phonenumber = phonenumber;
		cs.email = email;
		cs.referral = referral;
		
		return cs;
	}
}
