package com.ermolaev.db.model;

import com.ermolaev.common.ComString;

public class Counterparty {
	
	private Integer id;
	private String name;
	private String phones;
	private String email;
	private String website;
	private String city;
	private String streetname;
	private String number;
	private String room;
	private String postalcode;
	private Boolean deleted;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhones() {
		return phones;
	}
	public void setPhones(String phones) {
		this.phones = phones;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreetname() {
		return streetname;
	}
	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	public String getSiteName()
	{
		return ComString.getSiteNameFromURL(this.website);
	}
	
	public String getFnumber() {
		return (number.length() > 0) ? ", дом " + number : "";
	}
	public String getFroom() {
		return (room.length() > 0) ? ", кв." + room : "";
	}
	public String getFpostalcode() {
		return (postalcode.length() > 0) ? ", " + postalcode : "";
	}
	public String getFstreetname() {
		return (streetname.length() > 0) ? ", " + streetname : "";
	}
}
