package com.ermolaev.db.model;

import java.util.List;

import com.ermolaev.common.ComString;

public class Message {

	private Integer id;
	private String message;
	private String typeName;
	private Integer typeId;
	private Boolean ispattern;
	private Boolean available;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public Boolean getIspattern() {
		return ispattern;
	}
	public void setIspattern(Boolean ispattern) {
		this.ispattern = ispattern;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public String getShortMessage()
	{
		if(this.message.length() > 100)
			return ComString.clearHTML(this.message.substring(0, 100) + "...");
		else
			return ComString.clearHTML(this.message);
	}
}
