package com.ermolaev.db.model;

import java.sql.Date;
import java.sql.Time;

import com.ermolaev.common.ComString;
import com.ermolaev.common.MyCalendar;

public class Order {
	
	public static final int STATUS_WAITING = 1;
	public static final int STATUS_PROCESSING = 2;
	public static final int STATUS_SHIPPING = 3;
	public static final int STATUS_SHIPPED = 4; 
	public static final int STATUS_VIOLATION = 5;
	public static final int STATUS_UNPAID = 6;
	public static final int STATUS_FAIL = 7; 
	
	
	private Integer id;
	private Integer customerId;
	
	//message
	private String recipient;
	private String sender;
	private String message;
	private String comment;
	private Integer postcardId;
	
	//delivery time
	private Date deliveryDate;
	private Time startTime;
	private Time finishTime;
	
	private Integer complexity;
	private Float price;
	
	//address
	private String city;
	private String streetname;
	private String number;
	private String room;
	private String postalcode;
	
	private Integer status;
	private java.sql.Date tdate;
	private java.sql.Time ttime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public String getDeliveryDateS() {
		return deliveryDate.toString();
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Time getStartTime() {
		return startTime;
	}
	public String getStartTimeS() {
		return startTime.toString();
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public Time getFinishTime() {
		return finishTime;
	}
	public String getFinishTimeS() {
		return finishTime.toString();
	}
	public void setFinishTime(Time finishTime) {
		this.finishTime = finishTime;
	}
	public Integer getComplexity() {
		return complexity;
	}
	public void setComplexity(Integer complexity) {
		this.complexity = complexity;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreetname() {
		return streetname;
	}
	public void setStreetname(String streetname) {
		this.streetname = streetname;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getRoom() {
		return room;
	}
	public void setRoom(String room) {
		this.room = room;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public java.sql.Date getTdate() {
		return tdate;
	}
	public void setTdate(java.sql.Date tdate) {
		this.tdate = tdate;
	}
	public java.sql.Time getTtime() {
		return ttime;
	}
	public void setTtime(java.sql.Time ttime) {
		this.ttime = ttime;
	}
	
	public Integer getPostcardId() {
		return postcardId;
	}
	public void setPostcardId(Integer postcardId) {
		this.postcardId = postcardId;
	}
	
	public String getFormatStatus()
	{
		switch(this.status)
		{
			case STATUS_WAITING : return "ui-status-waiting";
			case STATUS_PROCESSING : return "ui-status-processing";
			case STATUS_SHIPPING : return "ui-status-shipping";
			case STATUS_SHIPPED : return "ui-status-shipped";
			case STATUS_VIOLATION : return "ui-status-violation";
			case STATUS_UNPAID : return "ui-status-unpaid";
			case STATUS_FAIL : return "ui-status-fail";
			default: return "ERROR";
		}
	}
	
	public String getFormatStartTime()
	{
		MyCalendar cal = new MyCalendar();
		cal.setTimeInMillis(this.startTime.getTime());
		cal.setMinute(0);
		return cal.getSqlTime().toString();
	}
	
	public String getTextStatus()
	{
		switch(this.status)
		{
			case STATUS_WAITING : return "Ожидает";
			case STATUS_PROCESSING : return "Обрабатывается";
			case STATUS_SHIPPING : return "Доставляется";
			case STATUS_SHIPPED : return "Выполнено!";
			case STATUS_VIOLATION : return "Нарушение";
			case STATUS_UNPAID : return "Неоплачено";
			case STATUS_FAIL : return "Не выполнено";
			default: return "ERROR";
		}
	}	
	public Order copy()
	{
		Order or = new Order();
		or.id = this.id;
		or.customerId = this.customerId;
		or.recipient = this.recipient;
		or.sender = this.sender;
		or.message = this.message;
		or.comment = this.comment;
		or.deliveryDate = this.deliveryDate;
		or.startTime = this.startTime;
		or.finishTime = this.finishTime;
		or.complexity = this.complexity;
		or.price = this.price;
		or.city = this.city;
		or.streetname = this.streetname;
		or.number = this.number;
		or.room = this.room;
		or.postalcode = this.postalcode;
		or.status = this.status;
		or.tdate = this.tdate;
		or.ttime = this.ttime;
		
		return or;
	}
}