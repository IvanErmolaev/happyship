package com.ermolaev.db.model;

import java.io.Serializable;

import com.ermolaev.web.PresentTypeBean;

public class PresentType {
	
	private Integer id;
	private Integer order;
	private String name;
	private Boolean available;
	private Boolean deleted;
	
	public PresentType() {}
	
	public PresentType(String name, Boolean available, Boolean deleted)
	{
		this.name = name;
		this.available = available;
		this.deleted = deleted;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	public Boolean getDeleted() {
		return deleted;
	}
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
}
