package com.ermolaev.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.transaction.Transaction;

import com.ermolaev.db.model.Customer;
import com.ermolaev.db.model.Order;

public interface OrderMapper {

	public List<Order> getOrders(
			@Param("date") Boolean date,
			@Param("status") Boolean status
	);
	
	public Integer getLastOrderByStatus(
			@Param("status") Integer status
	);
	
	public List<Order> getOrdersByStatus(
			@Param("orderStatus") Integer orderStatus
	);
	
	public Order getOrderById(
			@Param("orderId") Integer orderId
	);
	
	public Integer addOrder(
			@Param("referral") String referral,
			@Param("order") Order order,
			@Param("customer") Customer customer
	);
	
	public void changeStatus(
			@Param("id") Integer id, 
			@Param("status") Integer status
	);
	
	public void addSelectedPresent(
			@Param("orderId") Integer orderId,
			@Param("presentId") Integer presentId
	);
	
	public void deleteOrderById(
			@Param("orderId") Integer orderId
	);
	
/////////////////////////////////////////////////////////////////////
	/// CUSTOMERS
	public List<Customer> getCustomers();
	
	public Customer getCustomerById(
			@Param("id") Integer id
	);
	
	public Customer getCustomerByOrder(
			@Param("orderId") Integer orderId
	);
	
	public void deleteByCommentTest();
}
