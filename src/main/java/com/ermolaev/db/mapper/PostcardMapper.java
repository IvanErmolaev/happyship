package com.ermolaev.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ermolaev.db.model.Postcard;

public interface PostcardMapper {

	public List<Postcard> getAllPostcards();
	public List<Postcard> getPostcards(
			@Param("available") Boolean available,
			@Param("deleted") Boolean deleted
	);
	public Postcard getPostcardByOrder(
			@Param("orderId") Integer orderId
	);
	
	public void addPostcard(
			@Param("name") String name,
			@Param("image") String image,
			@Param("available") Boolean available
	);
	
	public void updatePostcard(
			@Param("id") Integer id,
			@Param("name") String name,
			@Param("image") String image,
			@Param("available") Boolean available
	);
	
	public void deletePostcard(
			@Param("id") Integer id
	);
	
	public void deleteTestPostcard();
}
