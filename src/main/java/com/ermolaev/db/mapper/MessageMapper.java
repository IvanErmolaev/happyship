package com.ermolaev.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ermolaev.db.model.Message;
import com.ermolaev.db.model.MessageType;

public interface MessageMapper {
	
	public List<Message> getAllMessages();
	public List<Message> getRealMessages();
	
	public List<Message> getMessages(
			@Param("available") Boolean available
	);
	
	public List<Message> getMessageById(
			@Param("id") Integer id
	);
	
	public List<Message> getMessagesByType(
			@Param("typeId") Integer typeId,
			@Param("available") Boolean available
	);
	
	public void addMessage(
			@Param("message") String message,
			@Param("typeId") Integer typeId,
			@Param("ispattern") Boolean ispattern,
			@Param("available") Boolean available
	);
	
	public void updateMessage(
			@Param("id") Integer id,
			@Param("message") String message,
			@Param("typeId") Integer typeId,
			@Param("ispattern") Boolean ispattern,
			@Param("available") Boolean available
	);
	
	public void deleteMessage(
			@Param("id") Integer id
	);
	
/////////////////////////////////////////////////////////////////////////////////
	// MESSAGE TYPES
/////////////////////////////////////////////////////////////////////////////////
	
	public List<MessageType> getAllMessageTypes();
	public List<MessageType> getRealMessageTypes();
	
	public List<MessageType> getMessageTypes(
			@Param("available") Boolean available,
			@Param("deleted") Boolean deleted
	);
	
	public List<MessageType> getMessageTypeById(
			@Param("id") Integer id
	);
	
	public void addMessageType(
			@Param("name") String name,
			@Param("description") String description,
			@Param("available") Boolean available
	);
	
	public void updateMessageType(
			@Param("id") Integer id,
			@Param("name") String name,
			@Param("description") String description,
			@Param("available") Boolean available
	);
	
	public void deleteMessageType(
			@Param("id") Integer id
	);
}
