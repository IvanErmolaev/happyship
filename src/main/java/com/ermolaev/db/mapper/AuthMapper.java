package com.ermolaev.db.mapper;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ermolaev.db.model.Session;

public interface AuthMapper {
	
	public Integer getAuth(
			@Param("hashCode") String hashCode
	);
	
	public List<Session> getSession(
			@Param("limit") Integer limit
	);
	
	public List<Session> getFailSession(
			@Param("limit") Integer limit
	);
	
	public void addSession(
			@Param("date") Date date,
			@Param("time") Time time,
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("ipAddress") String ipAddress
	);
	
	public void addFailSession(
			@Param("date") Date date,
			@Param("time") Time time,
			@Param("firstName") String firstName,
			@Param("lastName") String lastName,
			@Param("ipAddress") String ipAddress
	);
}
