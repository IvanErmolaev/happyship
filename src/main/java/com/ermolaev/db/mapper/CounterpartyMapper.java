package com.ermolaev.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ermolaev.db.model.Counterparty;

public interface CounterpartyMapper {

	public List<Counterparty> getAllCounterparties();
	
	public List<Counterparty> getCounterparties(
			@Param("deleted") Boolean deleted
	);
	
	public Counterparty getCounterpartyById(
			@Param("id") Integer id
	);
	
	public List<Counterparty> getCpartiesByPresent(
			@Param("presentId") Integer presentId,
			@Param("deleted") Boolean deleted
	);
	
	public List<Counterparty> getAllCpartiesByPresent(
			@Param("presentId") Integer presentId
	);	
	
	public Counterparty getLastCounterparty(
			@Param("deleted") Boolean deleted
	);
	
	public void addPresentToCparty(
			@Param("presentId") Integer presentId,
			@Param("cpartyId") Integer cpartyId
	);
	
	public void deletePresentFromCparty(
			@Param("presentId") Integer presentId,
			@Param("cpartyId") Integer cpartyId
	);
	
	public void restoreCounterparty(
			@Param("id") Integer id
	);
	
	public void addCounterparty(
			@Param("name") String name,
			@Param("phones") String phones,
			@Param("email") String email,
			@Param("website") String website,
			@Param("city") String city,
			@Param("streetname") String streetname,
			@Param("number") String number,
			@Param("room") String room,
			@Param("postalcode") String postalcode
	);
	
	public void updateCounterparty(
			@Param("id") Integer id,
			@Param("name") String name,
			@Param("phones") String phones,
			@Param("email") String email,
			@Param("website") String website,
			@Param("city") String city,
			@Param("streetname") String streetname,
			@Param("number") String number,
			@Param("room") String room,
			@Param("postalcode") String postalcode
	);
	
	public void removeAllPresents(
			@Param("cpartyId") Integer cpartyId
	);
	
	public void deleteCounterparty(
			@Param("id") Integer id
	);
}
