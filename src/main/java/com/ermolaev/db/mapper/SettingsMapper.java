package com.ermolaev.db.mapper;

import org.apache.ibatis.annotations.Param;

public interface SettingsMapper {
	
	public String getParameter(@Param("key") String key);
	public Integer getCountParameter(@Param("key") String key);
	public void addParameter(@Param("key") String key, @Param("value") String value);
	public void setParameter(@Param("key") String key, @Param("value") String value);
}
