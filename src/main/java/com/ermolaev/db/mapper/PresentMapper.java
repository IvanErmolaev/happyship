package com.ermolaev.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ermolaev.db.model.Present;
import com.ermolaev.db.model.PresentType;

public interface PresentMapper {
	
	public List<Present> getRealPresents();
	public List<Present> getAllPresents();
	
	public List<Present> getPresents(
			@Param("available") Boolean available, 
			@Param("deleted")Boolean deleted
	);
	
	public Present getPresentById(
			@Param("id") Integer id
	);
	
	public List<Present> getPresentsByOrder(
			@Param("orderId") Integer orderId
	);
	
	public void addPresentToCounterparty(
			@Param("presentId") Integer presentId,
			@Param("cpartyId") Integer cpartyId
	);
	
	public List<Present> getPresentByCparty(
			@Param("cpartyId") Integer cpartyId,
			@Param("deleted") Boolean deleted
	);
	
	public List<Present> getAllPresentByCparty(
			@Param("cpartyId") Integer cpartyId
	);
	
	public void deletePresentFromCounterparty(
			@Param("presentId") Integer presentId,
			@Param("cpartyId") Integer cpartyId
	);
	
	public Present getLastPresent(
			@Param("deleted") Boolean deleted
	);
	
	public void restorePresent(
			@Param("id") Integer id
	);
	
	public void addPresent(
			@Param("name") String name,
			@Param("description") String description,
			@Param("imageurl") String imageurl,
			@Param("typeId") Integer typeId,
			@Param("complexity") Integer complexity,
			@Param("amount") Integer amount,
			@Param("price") Float price,
			@Param("available") Boolean available
	);
	
	public void updatePresent(
			@Param("id") Integer id,
			@Param("name") String name,
			@Param("description") String description,
			@Param("imageurl") String imageurl,
			@Param("typeId") Integer typeId,
			@Param("complexity") Integer complexity,
			@Param("amount") Integer amount,
			@Param("price") Float price,
			@Param("available") Boolean available
	);
	
	public void deletePresent(
			@Param("id") Integer id
	);
	
///////////////////////////////////////////////////////////////////////////////
	// PRESENT TYPE
///////////////////////////////////////////////////////////////////////////////	
	public List<PresentType> getAllPresentTypes();
	public List<PresentType> getRealPresentTypes();
	
	public List<PresentType> getPresentTypes(
			@Param("available") Boolean available, 
			@Param("deleted") Boolean deleted
	);
	
	public PresentType getPresentTypesById(
			@Param("id") Integer id
	);
	
	public List<PresentType> getPresentTypesByOrder(
			@Param("available") Boolean available, 
			@Param("deleted") Boolean deleted
	);
	
	public List<Present> getPresentsByType(
			@Param("typeId")Integer typeId, 
			@Param("available")Boolean available,
			@Param("deleted") Boolean deleted
	);
	
	public void addPresentType(
			@Param("name") String name,
			@Param("order") Integer order,
			@Param("available") Boolean available
	);
	
	public void updatePresentType(
			@Param("id") Integer id,
			@Param("name") String name,
			@Param("order") Integer order,
			@Param("available") Boolean available
	);
	
	public void deletePresentType(
			@Param("id") Integer id
	);
}
