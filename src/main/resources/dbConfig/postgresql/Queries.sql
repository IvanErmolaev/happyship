CREATE TABLE t_counterparties (
	cpt_id SERIAL PRIMARY KEY,
	cpt_name VARCHAR(100) NOT NULL,
	cpt_phones VARCHAR(400) NOT NULL,
	cpt_email VARCHAR(400),
	cpt_website VARCHAR(400),
	cpt_city VARCHAR(100),
	cpt_streetname VARCHAR(200) NOT NULL,
	cpt_number VARCHAR(5) NOT NULL,
	cpt_room VARCHAR(10),
	cpt_postalcode VARCHAR(16) NOT NULL,
	cpt_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_present_types(
	ptp_id SERIAL PRIMARY KEY,
	ptp_name VARCHAR(300) NOT NULL,
	ptp_available BOOLEAN NOT NULL,
	ptp_order INT DEFAULT 1,
	ptp_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_postcards(
	psc_id SERIAL PRIMARY KEY,
	psc_name VARCHAR(300) NOT NULL,
	psc_image VARCHAR(700) NOT NULL,
	psc_available BOOLEAN DEFAULT false,
	psc_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_presents (
	pr_id SERIAL PRIMARY KEY,
	pr_name VARCHAR(300) NOT NULL,
	pr_description VARCHAR NOT NULL,
	pr_imageurl VARCHAR NOT NULL,
	pr_type_id INT references t_present_types(ptp_id),
	pr_complexity INT NOT NULL,
	pr_amount INT NOT NULL,
	pr_price NUMERIC NOT NULL,
	pr_available BOOLEAN NOT NULL,
	pr_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_message_types(
	mtp_id SERIAL PRIMARY KEY,
	mtp_name VARCHAR(200) NOT NULL,
	mtp_description VARCHAR NOT NULL,
	mtp_available BOOLEAN NOT NULL,
	mtp_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_messages(
	ms_id SERIAL PRIMARY KEY,
	ms_message VARCHAR(1000) NOT NULL,
	ms_type_id INT references t_message_types(mtp_id),
	ms_ispattern BOOLEAN NOT NULL,
	ms_available BOOLEAN NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_customers(
	cs_id SERIAL PRIMARY KEY,
	cs_firstname VARCHAR(100) NOT NULL,
	cs_lastname VARCHAR(100) NOT NULL,
	cs_middlename VARCHAR(100),
	cs_phonenumber VARCHAR(11) NOT NULL,
	cs_email VARCHAR(100),
	cs_referral VARCHAR(100)
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_runners(
	rn_id SERIAL PRIMARY KEY,
	rn_firstname VARCHAR(100) NOT NULL,
	rn_lastname VARCHAR(100) NOT NULL,
	rn_middlename VARCHAR(100) NOT NULL,
	rn_phonenumber VARCHAR(16) NOT NULL,
	rn_city VARCHAR(100),
	rn_streetname VARCHAR(200) NOT NULL,
	rn_number VARCHAR(5) NOT NULL,
	rn_room VARCHAR(10),
	rn_postalcode VARCHAR(5) NOT NULL,
	rn_ptransport BOOLEAN NOT NULL,
	rn_deleted BOOLEAN DEFAULT false
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_dayintervals(
	dn_id SERIAL PRIMARY KEY,
	dn_weekday VARCHAR(2) NOT NULL,
	dn_other_id INT,
	dn_start TIME NOT NULL,
	dn_finish TIME NOT NULL,
	dn_type INT NOT NULL
);


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_orders(
	or_id SERIAL PRIMARY KEY,
	or_referral VARCHAR(100),
	or_customer_id INT references t_customers(cs_id),
	or_recipient VARCHAR(30),
	or_sender VARCHAR(30),
	or_message VARCHAR(600),
	or_comment VARCHAR(1000),
	or_postcard_id INT references t_postcards(psc_id),
	or_deliveryDate DATE NOT NULL,
	or_startTime TIME NOT NULL,
	or_finishTime TIME NOT NULL,
	or_complexity INT NOT NULL,
	or_price NUMERIC NOT NULL,
	or_city VARCHAR(100) NOT NULL,
	or_streetname VARCHAR(200) NOT NULL,
	or_number VARCHAR(5) NOT NULL,
	or_room VARCHAR(10) NOT NULL,
	or_postalcode VARCHAR(5) NOT NULL,
	or_date DATE NOT NULL,
	or_time TIME NOT NULL,
	or_status INT NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_present_places (
	ppl_counterparty_id INT references t_counterparties(cpt_id),
	ppl_present_id INT references t_presents(pr_id),
	PRIMARY KEY(ppl_counterparty_id, ppl_present_id)
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_selected_presents(
	spr_order_id INT references t_orders(or_id),
	spr_present_id INT references t_presents(pr_id),
	PRIMARY KEY(spr_order_id, spr_present_id)
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_referrals(
	rf_id SERIAL PRIMARY KEY,
	rf_name varchar(100) NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_security(
	scr_id SERIAL PRIMARY KEY,
	scr_hash VARCHAR(128) NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_sessions(
	ss_id SERIAL PRIMARY KEY,
	ss_date DATE NOT NULL,
	ss_time TIME NOT NULL,
	ss_firstName VARCHAR(30) NOT NULL,
	ss_lastName VARCHAR(30) NOT NULL,
	ss_ip VARCHAR(15) NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_failSessions(
	fs_id SERIAL PRIMARY KEY,
	fs_date DATE NOT NULL,
	fs_time TIME NOT NULL,
	fs_firstName VARCHAR(30) NOT NULL,
	fs_lastName VARCHAR(30) NOT NULL,
	fs_ip VARCHAR(15) NOT NULL
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE TABLE t_settings(
	st_key VARCHAR(100) PRIMARY KEY,
	st_value VARCHAR(1000)
);

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deleteCounterparty(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_present_places WHERE ppl_counterparty_id = id);
BEGIN
	IF count > 0 THEN
		UPDATE t_counterparties SET cpt_deleted = true WHERE cpt_id = id;
	ELSE
		DELETE FROM t_counterparties WHERE cpt_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deletePresentType(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_presents WHERE pr_type_id = id);
BEGIN
	IF count > 0 THEN
		UPDATE t_present_types SET ptp_deleted = true WHERE ptp_id = id;
	ELSE
		DELETE FROM t_present_types WHERE ptp_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deletePresent(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_selected_presents WHERE spr_present_id = id);
BEGIN
	count := count + (SELECT count(*) FROM t_present_places WHERE ppl_present_id = id);
	IF count > 0 THEN
		UPDATE t_presents SET pr_deleted = true WHERE pr_id = id;
	ELSE
		DELETE FROM t_presents WHERE pr_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deleteMessage(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_orders WHERE or_message_id = id);
BEGIN
	IF count > 0 THEN
		UPDATE t_messages SET ms_deleted = true WHERE ms_id = id;
	ELSE
		DELETE FROM t_messages WHERE ms_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deleteMessageType(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_messages WHERE ms_type_id = id);
BEGIN
	IF count > 0 THEN
		UPDATE t_message_types SET mtp_deleted = true WHERE mtp_id = id;
	ELSE
		DELETE FROM t_message_types WHERE mtp_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deletePostcard(id INT)
RETURNS void AS $$
DECLARE count int = (SELECT count(*) FROM t_orders WHERE or_postcard_id = id);
BEGIN
	IF count > 0 THEN
		UPDATE t_postcards SET psc_deleted = true WHERE psc_id = id;
	ELSE
		DELETE FROM t_postcards CASCADE WHERE psc_id = id;
	END IF;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION addOrder(
	_referral VARCHAR,
	_recipient VARCHAR,
	_sender VARCHAR,
	_message VARCHAR,
	_comment VARCHAR,
	_postcard_id INT,
	_deliveryDate DATE,
	_startTime TIME,
	_finishTime TIME,
	_complexity INT,
	_price NUMERIC,
	_city VARCHAR,
	_streetname VARCHAR,
	_number VARCHAR,
	_room VARCHAR,
	_postalcode VARCHAR,
	_firstname VARCHAR,
	_lastname VARCHAR,
	_middlename VARCHAR,
	_phonenumber VARCHAR,
	_email VARCHAR,
	_date DATE,
	_time TIME
)
RETURNS void AS $$
DECLARE 
	customerId INT = 0;
BEGIN
	--ADD CUSTOMER
	customerId := (SELECT cs_id FROM t_customers WHERE cs_phonenumber = _phonenumber);
	IF customerId IS NULL THEN
		INSERT INTO t_customers(
			cs_firstname,
			cs_lastname,
			cs_middlename,
			cs_phonenumber,
			cs_email,
			cs_referral
			) VALUES (
				_firstname,
				_lastname,
				_middlename,
				_phonenumber,
				_email,
				genReferral()
			);
		--get new customer id
		customerId := (SELECT cs_id FROM t_customers WHERE cs_phonenumber = _phonenumber);
	END IF;
	
	--ADD ORDER
	INSERT INTO t_orders (
		or_referral,
		or_customer_id,
		or_recipient,
		or_sender,
		or_message,
		or_comment,
		or_postcard_id,
		or_deliveryDate,
		or_startTime,
		or_finishTime,
		or_complexity,
		or_price,
		or_city,
		or_streetname,
		or_number,
		or_room,
		or_postalcode,
		or_date,
		or_time,
		or_status
	) VALUES (
		_referral,
		customerId,
		_recipient,
		_sender,
		_message,
		_comment,
		_postcard_id,
		_deliveryDate,
		_startTime,
		_finishTime,
		_complexity,
		_price,
		_city,
		_streetname,
		_number,
		_room,
		_postalcode,
		_date,
		_time,
		6 --STATUS
	);
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION addSelectedPresent(orderId INT, presentId INT)
RETURNS void AS $$
BEGIN
	UPDATE t_presents SET pr_amount = pr_amount - 1 WHERE pr_id = presentId;
	INSERT INTO t_selected_presents(spr_order_id, spr_present_id)
	VALUES(orderId, presentId);
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deleteOrder(orderId INT)
RETURNS void AS $$
DECLARE	
	pid INT;
BEGIN
	-- Get all order's presents
	FOR pid IN SELECT 
					pr_id 
				FROM 
					t_selected_presents 
					JOIN t_presents ON pr_id = spr_present_id
				WHERE
					spr_order_id = orderId
	LOOP
		-- Free amount this present
		UPDATE t_presents SET pr_amount = pr_amount + 1 WHERE pr_id = pid;
		--delete pair
		DELETE FROM t_selected_presents CASCADE WHERE spr_order_id = orderId AND spr_present_id = pid;
	END LOOP;
	DELETE FROM t_orders CASCADE WHERE or_id = orderId;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION deleteOrder(orderId INT)
RETURNS void AS $$
DECLARE	
	pid INT;
BEGIN
	-- Get all order's presents
	FOR pid IN SELECT 
					pr_id 
				FROM 
					t_selected_presents 
					JOIN t_presents ON pr_id = spr_present_id
				WHERE
					spr_order_id = orderId
	LOOP
		-- Free amount this present
		UPDATE t_presents SET pr_amount = pr_amount + 1 WHERE pr_id = pid;
		--delete pair
		DELETE FROM t_selected_presents CASCADE WHERE spr_order_id = orderId AND spr_present_id = pid;
	END LOOP;
	DELETE FROM t_orders CASCADE WHERE or_id = orderId;
END;
$$ LANGUAGE plpgsql;

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION genReferral()
RETURNS TEXT AS $$
DECLARE 
	refCount INT := (SELECT COUNT(*)-1 FROM t_referrals);
	code1 INT := (refCount * random())::INT+1;
	code2 INT := (refCount * random())::INT+1;
	codeName1 TEXT;
	codeName2 TEXT;
	resultName TEXT; 
BEGIN
	IF code1 = 0 THEN
		code1 := code1 + 1;
	END IF;
	IF code2 = 0 THEN
		code2 := code2 + 1;
	END IF;
	codeName1 := (SELECT rf_name FROM t_referrals WHERE rf_id = code1);
	codeName2 := (SELECT rf_name FROM t_referrals WHERE rf_id = code2);
	resultName := codeName1 || codeName2;
	RETURN resultName::TEXT || (SELECT COUNT(*)+1 FROM t_customers WHERE cs_referral LIKE resultName || '%')::TEXT;
END;
$$ LANGUAGE plpgsql;

INSERT INTO t_referrals(rf_name) VALUES ('Rose');
INSERT INTO t_referrals(rf_name) VALUES ('Dog');
INSERT INTO t_referrals(rf_name) VALUES ('Cat');
INSERT INTO t_referrals(rf_name) VALUES ('Apple');
INSERT INTO t_referrals(rf_name) VALUES ('Puppy');
INSERT INTO t_referrals(rf_name) VALUES ('Sun');