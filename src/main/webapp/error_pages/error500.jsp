<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
	@import url(http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,cyrillic);
	@import url(http://fonts.googleapis.com/css?family=Exo+2:500&amp;subset=latin,cyrillic);
	
	BODY {
		background: #FBF5EF;
	}
	h2 {
		font-family: 'Open Sans', sans-serif;
		font-size:20px;
		color:#2E2E2E;
	}
</style>
<center>
	<div style="height:30%;" border="0"> </div>
	<table>
		<tr>
			<td><img src="images/warning.png" width="200"></td>
			<td width="300"><h2>Что-то пошло не так... <br>Мы просим у вас прощения!</h2></td>
		</tr>
	</table>
</center>