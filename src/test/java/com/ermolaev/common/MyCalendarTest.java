package com.ermolaev.common;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MyCalendarTest {
	
	@Test
	public void constructorsShouldWork()
	{
		MyCalendar cal = new MyCalendar(2015, 2, 24, 14, 20);
		assertTrue(cal.getSqlDate().toString().equals("2015-02-24")); //test date
		assertTrue(cal.getSqlTime().toString().equals("14:20:00")); //test time
		
		cal.addDate(10);
		assertTrue(cal.getSqlDate().toString().equals("2015-03-06"));
	}
	
	@Test
	public void addDateShouldWork()
	{
		MyCalendar cal = new MyCalendar(2015, 2, 24, 14, 20);
		
		cal.addDate(10);
		assertTrue(cal.getSqlDate().toString().equals("2015-03-06"));
	}
	
	@Test
	public void addHoursShouldWork()
	{
		MyCalendar cal = new MyCalendar(2015, 2, 24, 14, 20);
		
		cal.addHours(47);
		assertTrue(cal.getSqlDate().toString().equals("2015-02-26")); //test date
		assertTrue(cal.getSqlTime().toString().equals("13:20:00")); //test time
	}
	
	@Test
	public void setDateShouldWorks()
	{
		MyCalendar cal = new MyCalendar(2015, 4, 11);
		cal.setDate(2017, 11, 23);
		assertTrue(cal.getSqlDate().toString().equals("2017-11-23")); //test date
	}

	@Test
	public void setTimeShouldWorks()
	{
		MyCalendar cal = new MyCalendar(2015, 4, 11, 9,23, 56);
		cal.setTime(16, 51, 5);
		assertTrue(cal.getSqlTime().toString().equals("16:51:05")); //test time
	}
	
	@Test
	public void setNumberOfDateShouldWork()
	{
		MyCalendar cal = new MyCalendar(2015, 4, 11, 9,23, 56);
		cal.setDayOfMonth(29);
		assertTrue(cal.getSqlDate().toString().equals("2015-04-29")); //test date
		cal.setMonth(10);
		assertTrue(cal.getSqlDate().toString().equals("2015-10-29")); //test date
		cal.setYear(2025);
		assertTrue(cal.getSqlDate().toString().equals("2025-10-29")); //test date
	}
	
	@Test
	public void setNumberOfTimeShouldWork()
	{
		MyCalendar cal = new MyCalendar(2015, 4, 11, 9,23, 56);
		cal.setHours(19);
		assertTrue(cal.getSqlTime().toString().equals("19:23:56")); //test time
		cal.setMinute(5);
		assertTrue(cal.getSqlTime().toString().equals("19:05:56")); //test time
		cal.setSecond(55);
		assertTrue(cal.getSqlTime().toString().equals("19:05:55")); //test time
	}
}
