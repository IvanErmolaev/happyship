package com.ermolaev.common;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map.Entry;

import org.junit.Test;

import com.ermolaev.web.OrderBean;

public class ComTimeTest {
	
	@Test
	public void timeShouldAvailableBeforeTime()
	{
		Calendar c = new GregorianCalendar(2015, 3, 10, 7, 20, 0);
		ComTime time1 = new ComTime(c.getTime());
		for(Entry<java.sql.Time, Boolean> it : time1.getTimes())
		{
			assertFalse(it.getValue());	
		}
	}
	
	@Test
	public void shouldBeTomorrow()
	{
		Calendar c = new GregorianCalendar(2015, 7, 27, 21, 16, 0);
		ComTime time1 = new ComTime(c.getTime());
		assertTrue(time1.getDays().get(0).getValue().equals("Завтра"));
	}
	
	@Test
	public void shouldBeToday()
	{
		Calendar c = new GregorianCalendar(2015, 3, 10, 15, 20, 0);
		ComTime time1 = new ComTime(c.getTime());
		assertTrue(time1.getDays().get(0).getValue().equals("Сегодня"));
	}
	
	@Test
	public void shouldBeFormatDate()
	{
		Calendar c = new GregorianCalendar(2015, 9, 28, 9, 5, 0);

		ComTime time1 = new ComTime(c.getTime());
		
		OrderBean order = new OrderBean();
		order.setTime(time1);
		
		DateFormat df = new SimpleDateFormat("d");

		for(int i = 0; i < time1.getDays().size(); i++)
		{
			Entry<java.sql.Date, String> it = time1.getDays().get(i);
			order.setDeliveryTime(0);
			order.setDeliveryDate(i);
			
			c.clear();
			c.setTime(it.getKey());
			String dateStr = c.get(Calendar.DATE) + " " + ComString.getMonthNameFor(c.get(Calendar.MONTH)) + " с 08:00 до 10:00";
			assertTrue(order.getSelectedDate().equals(dateStr));
		}
	}
}
