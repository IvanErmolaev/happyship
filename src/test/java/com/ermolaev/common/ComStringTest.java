package com.ermolaev.common;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ComStringTest {

	@Test
	public void clearHTMLpatterShouldWork()
	{
		String testString1 = "<a href=\"http://mylink.com/link/something/type.php?rude&cole=20+11%tyno;\">My <b>text</b><br> here <i>type</i></a>";
		String testString2 = "<div style='font-weight:20px;color:red;' width=\"200\">This is </div>my text!<br />";
		String testString3 = "<br>11<br />Red:?k<hr>";
		
		assertTrue(ComString.clearHTML(testString1).equals("My text here type"));
		assertTrue(ComString.clearHTML(testString2).equals("This is my text!"));
		assertTrue(ComString.clearHTML(testString3).equals("11Red:?k"));
		
		System.out.println(Crypto.getMD5("checkOrder;87.10;643;1001;13;55;8123294469;s<kY23653f,{9fcnshwq"));
	}
}
