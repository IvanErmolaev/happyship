package com.ermolaev.db;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class DatabaseRunner {

	public static void main(String args[])
	{
		Result result = JUnitCore.runClasses(DatabaseTest.class);
		for(Failure failure : result.getFailures())
		{
			System.out.println(failure.toString());
		}
	}
}
