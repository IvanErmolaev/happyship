package com.ermolaev.db;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.transaction.Transaction;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ermolaev.common.MyCalendar;
import com.ermolaev.db.mapper.CounterpartyMapper;
import com.ermolaev.db.mapper.MessageMapper;
import com.ermolaev.db.mapper.OrderMapper;
import com.ermolaev.db.mapper.PostcardMapper;
import com.ermolaev.db.mapper.PresentMapper;
import com.ermolaev.db.model.Counterparty;
import com.ermolaev.db.model.Customer;
import com.ermolaev.db.model.Message;
import com.ermolaev.db.model.MessageType;
import com.ermolaev.db.model.Order;
import com.ermolaev.db.model.Postcard;
import com.ermolaev.db.model.Present;
import com.ermolaev.db.model.PresentType;

public class DatabaseTest {

	private static TestConnection conn;
	
	@BeforeClass
	public static void init()
	{
		conn = new TestConnection();		
	}
	
	@AfterClass
	public static void finish()
	{
		conn = null;
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
	/// MESSAGES
//////////////////////////////////////////////////////////////////////////////////////////////	
	@Test
	public void messageCRUDshouldWork()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			MessageMapper mapper = session.getMapper(MessageMapper.class);
			
			//Data definition
			String name = "TestMessageType";
			String description = "TestMessageTypeDescription";
			String message = "TestMessage";
			
			//insert data
			mapper.addMessageType(name, description, true);
			session.commit();
			
			//get data
			List<MessageType> listOfTypes = mapper.getRealMessageTypes();
			assertNotNull(listOfTypes);
			
			//get last MessageType for test
			MessageType lastType = listOfTypes.get(0);
			
			
			//Field asserts
			assertTrue(lastType.getName().equals(name));
			assertTrue(lastType.getDescription().equals(description));
			
			/////////////////////////////////////////////////////////////////
			/// ADD NEW MESSAGE
			mapper.addMessage(message, lastType.getId(), true, true);
			session.commit();
			
			//get data
			List<Message> listOfMessages = mapper.getRealMessages();
			assertNotNull(listOfMessages);
			assertTrue(listOfMessages.size() > 0);
			
			//get last MessageType for test
			Message lastMessages = listOfMessages.get(0);
			
			//Field asserts
			assertTrue(lastMessages.getMessage().equals(message));
			assertTrue(lastMessages.getTypeId().equals(lastType.getId()));
			
			
			///////////////////////////////////////////////////////////////
			/// DELETE
			mapper.deleteMessageType(lastType.getId());
			session.commit();
			
			//get list of types
			listOfTypes = mapper.getAllMessageTypes();
			assertNotNull(listOfTypes);
			
			//get last element
			lastType = listOfTypes.get(0);
			
			//Field asserts
			assertTrue(lastType.getName().equals(name));
			assertTrue(lastType.getDescription().equals(description));
			assertTrue(lastType.getDeleted() == true);
			
			//delete message and delete type message(from database)
			mapper.deleteMessage(lastMessages.getId());
			mapper.deleteMessageType(lastType.getId());
			session.commit();
						
			//get list of types
			listOfTypes = mapper.getAllMessageTypes();
			assertNotNull(listOfTypes);
			
			//if list isn't empty
			if(listOfTypes.size() > 0)
			{
				//get last element
				Integer deletedId = lastType.getId();
				lastType = listOfTypes.get(listOfTypes.size()-1);	
				//Field asserts
				assertFalse(lastType.getId() == deletedId);
			}
		}
		finally
		{
			session.close();
		}
	}

	
//////////////////////////////////////////////////////////////////////////////////////////////
	/// PRESENTS
//////////////////////////////////////////////////////////////////////////////////////////////
	@Test
	public void presentCRUDshouldWork()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper mapper = session.getMapper(PresentMapper.class);
			
			//Default Present Type
			PresentType defType = new PresentType();
			defType.setName("PresentTypeNameTest");
			defType.setOrder(-256);
			
			//Default Present
			Present defPresent = new Present();
			defPresent.setName("PresentNameTest");
			defPresent.setDescription("PresentDescriptionTest");
			defPresent.setAmount(111);
			defPresent.setComplexity(22);
			defPresent.setPrice(256.25f);
			defPresent.setImageurl("http://url.com/image.png");

			//add present type
			mapper.addPresentType(defType.getName(), defType.getOrder(), true);
			session.commit();
			
			//get data
			List<PresentType> listOfTypes = mapper.getRealPresentTypes();
			assertNotNull(listOfTypes);
			assertTrue(listOfTypes.size() > 0);
			
			//get last MessageType for test
			PresentType lastType = listOfTypes.get(0);
			
			//fields asserts
			assertTrue(lastType.getName().equals(defType.getName()));
			assertTrue(lastType.getOrder()*(-1) == defType.getOrder()*(-1));
			assertTrue(lastType.getDeleted() == false);
			
			/* ADD PRESENT */
			mapper.addPresent(
					defPresent.getName(), 
					defPresent.getDescription(), 
					defPresent.getImageurl(), 
					lastType.getId(), 
					defPresent.getComplexity(), 
					defPresent.getAmount(), 
					defPresent.getPrice(), 
					true
			);
			session.commit();
			
			//get data
			List<Present> listOfPresents = mapper.getRealPresents();
			assertNotNull(listOfPresents);
			assertTrue(listOfPresents.size() > 0);
			
			//get last MessageType for test
			Present lastPresent = listOfPresents.get(0);
			System.out.println(lastPresent.getTypeId().compareTo(lastType.getId()));
			//fields asserts
			assertTrue(defPresent.getName().equals(lastPresent.getName()));
			assertTrue(defPresent.getDescription().equals(lastPresent.getDescription()));
			assertTrue(defPresent.getImageurl().equals(lastPresent.getImageurl()));
			assertTrue(lastPresent.getTypeId().compareTo(lastType.getId()) == 0);
			assertTrue(defPresent.getComplexity() == lastPresent.getComplexity());
			assertTrue(defPresent.getAmount() == lastPresent.getAmount());
			assertTrue(Math.abs(defPresent.getPrice()/lastPresent.getPrice()-1) < 1E-5);
		
			///////////////////////////////////////////////////////////////
			/// DELETE
			mapper.deletePresentType(lastType.getId());
			session.commit();
			
			//get data
			listOfTypes = mapper.getAllPresentTypes();
			assertNotNull(listOfTypes);
			assertTrue(listOfTypes.size() > 0);
			
			//get last element
			lastType = listOfTypes.get(0);
			
			//Field asserts
			assertTrue(lastType.getName().equals(defType.getName()));
			assertTrue(lastType.getOrder()*(-1) == defType.getOrder()*(-1));
			assertTrue(lastType.getDeleted() == true);

			//delete message and delete type message(from database)
			mapper.deletePresent(lastPresent.getId());
			mapper.deletePresentType(lastType.getId());
			session.commit();
						
			//get list of types
			listOfTypes = mapper.getAllPresentTypes();
			assertNotNull(listOfTypes);
			
			//if list isn't empty
			if(listOfTypes.size() > 0)
			{
				//get last element
				Integer deletedId = lastType.getId();
				lastType = listOfTypes.get(0);	
				//Field asserts
				assertFalse(lastType.getId() == deletedId);
			}
		}
		finally
		{
			session.close();
		}		
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////
	/// COUNTERPARTY
//////////////////////////////////////////////////////////////////////////////////////////////
	@Test
	public void counterpartyCRUDshouldWork()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			CounterpartyMapper mapper = session.getMapper(CounterpartyMapper.class);
			
			
			//Default data
			Counterparty defCp = new Counterparty();
			defCp.setName("A1a1A1a1A1a1A");
			defCp.setPhones("B1b1B1b1B1b1B");
			defCp.setEmail("C1c1C1c1C1c1C");
			defCp.setWebsite("D1d1D1d1D1d1D");
			defCp.setCity("E1e1E1e1E1e1E");
			defCp.setStreetname("F1f1F1f1");
			defCp.setNumber("G1g1");
			defCp.setRoom("H1h1H1h1");
			defCp.setPostalcode("J1j1J1j1J1j1J");
			
			//add to database
			mapper.addCounterparty(
					defCp.getName(), 
					defCp.getPhones(), 
					defCp.getEmail(), 
					defCp.getWebsite(), 
					defCp.getCity(), 
					defCp.getStreetname(), 
					defCp.getNumber(), 
					defCp.getRoom(), 
					defCp.getPostalcode()
			);
			session.commit();
			
			//get data
			List<Counterparty> listOfCparts = mapper.getCounterparties(false);
			assertNotNull(listOfCparts);
			assertTrue(listOfCparts.size() > 0);
			
			//get last element
			Counterparty lastCparty = listOfCparts.get(0);
			assertNotNull(lastCparty);
			
			//comparison
			assertTrue(defCp.getName().equals(lastCparty.getName()));
			assertTrue(defCp.getPhones().equals(lastCparty.getPhones()));
			assertTrue(defCp.getEmail().equals(lastCparty.getEmail()));
			assertTrue(defCp.getWebsite().equals(lastCparty.getWebsite()));
			assertTrue(defCp.getCity().equals(lastCparty.getCity()));
			assertTrue(defCp.getStreetname().equals(lastCparty.getStreetname()));
			assertTrue(defCp.getNumber().equals(lastCparty.getNumber()));
			assertTrue(defCp.getRoom().equals(lastCparty.getRoom()));
			assertTrue(defCp.getPostalcode().equals(lastCparty.getPostalcode()));
			
			//delete mapper
			mapper.deleteCounterparty(lastCparty.getId());
			session.commit();
			
			//get data
			listOfCparts = mapper.getCounterparties(false);
			assertNotNull(listOfCparts);
			
			if(listOfCparts.size() > 0)
			{
				Integer index = lastCparty.getId();
				//get last element
				lastCparty = listOfCparts.get(0);
				assertNotNull(lastCparty);
				
				//comparison
				assertTrue(lastCparty.getId() != index);
			}
		}
		finally
		{
			session.close();
		}		
	}

//////////////////////////////////////////////////////////////////////////////////////////////
	/// PRESENT AND COUNTERPARTY
//////////////////////////////////////////////////////////////////////////////////////////////
	@Test
	public void presentAndCounterpartyCRUD()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PresentMapper pMapper = session.getMapper(PresentMapper.class);
			CounterpartyMapper cMapper = session.getMapper(CounterpartyMapper.class);
			
			//default present type
			PresentType defType = new PresentType();
			defType.setName("PresentTypeNameTest");
			defType.setOrder(-256);
			
			//Default Present Type
			Present defPresent = new Present();
			defPresent.setName("PresentNameTest");
			defPresent.setDescription("PresentDescriptionTest");
			defPresent.setImageurl("http://url.com/image.png");
			defPresent.setTypeId(256);
			defPresent.setAmount(111);
			defPresent.setComplexity(22);
			defPresent.setPrice(256.25f);
			
			//Default data
			Counterparty defCp = new Counterparty();
			defCp.setName("A1a1A1a1A1a1A");
			defCp.setPhones("B1b1B1b1B1b1B");
			defCp.setEmail("C1c1C1c1C1c1C");
			defCp.setWebsite("D1d1D1d1D1d1D");
			defCp.setCity("E1e1E1e1E1e1E");
			defCp.setStreetname("F1f1F1f1");
			defCp.setNumber("G1g1");
			defCp.setRoom("H1h1H1h1");
			defCp.setPostalcode("J1j1J1j1J1j1J");
			
			/////////////////////////////////////////////////
			/// ADD PRESENT TYPE
			pMapper.addPresentType(
					defType.getName(), 
					defType.getOrder(), 
					true
			);
			session.commit();
			
// PresentMapper.getRealPresentTypes
			List<PresentType> listOfTypes = pMapper.getRealPresentTypes();
			assertNotNull(listOfTypes);
			assertTrue(listOfTypes.size() > 0);
			
			PresentType lastType = listOfTypes.get(0);
			assertTrue(lastType.getName().equals(defType.getName()));
			
// PresentMapper.addPresent			
			/////////////////////////////////////////////////
			/// ADD PRESENT
			pMapper.addPresent(
					defPresent.getName(), 
					defPresent.getDescription(), 
					defPresent.getImageurl(), 
					lastType.getId(), 
					defPresent.getComplexity(), 
					defPresent.getAmount(), 
					defPresent.getPrice(), 
					true
			);
			
// CounterpartyMapper.addCounterparty
			/////////////////////////////////////////////////
			/// ADD COUNTERPARTY
			cMapper.addCounterparty(
					defCp.getName(), 
					defCp.getPhones(), 
					defCp.getEmail(), 
					defCp.getWebsite(), 
					defCp.getCity(), 
					defCp.getStreetname(), 
					defCp.getNumber(), 
					defCp.getRoom(), 
					defCp.getPostalcode()
			);
			session.commit();
			
// PresentMapper.getRealPresents			
			List<Present> listOfPresent = pMapper.getRealPresents();
			
// CounterpartyMapper.getAllCounterparties
			List<Counterparty> listOfCparty = cMapper.getAllCounterparties();
			
			//the testing of lists
			assertNotNull(listOfPresent);
			assertNotNull(listOfCparty);
			assertTrue(listOfPresent.size() > 0);
			assertTrue(listOfCparty.size() > 0);

			//get last data
			Present lastPresent = listOfPresent.get(0);
			Counterparty lastCparty = listOfCparty.get(0);
			
			//test for null
			assertNotNull(lastPresent);
			assertNotNull(lastCparty);
			
			//test for name
			assertTrue(lastPresent.getName().equals(defPresent.getName()));
			assertTrue(lastCparty.getName().equals(defCp.getName()));
			
// PresentMapper.addPresentToCounterparty			
			/////////////////////////////////////////////////
			/// ADD PRESENT TO COUNTERPARTY RELATION
			pMapper.addPresentToCounterparty(lastPresent.getId(), lastCparty.getId());
			session.commit();

// CounterpartyMapper.getCpartiesByPresent
			//get from present places
			Counterparty bindCparty = cMapper.getCpartiesByPresent(lastPresent.getId(), false).get(0);
// PresentMapper.getPresentByCparty
			Present bindPresent = pMapper.getPresentByCparty(lastCparty.getId(), false).get(0);
			
			//test for null
			assertNotNull(bindCparty);
			assertNotNull(bindPresent);
			
			//test getCpartiesByPresent & getPresentByCparty
			assertTrue(bindCparty.getId().compareTo(lastCparty.getId()) == 0);
			assertTrue(bindPresent.getId().compareTo(lastPresent.getId()) == 0);

// PresentMapper.deletePresent
			//delete present
			pMapper.deletePresent(lastPresent.getId());
			session.commit();

// PresentMapper.getLastPresent
			//try get it
			Present lastDeletedPresent = pMapper.getLastPresent(true);
			assertTrue(lastDeletedPresent.getId().compareTo(lastPresent.getId()) == 0);
			assertTrue(lastDeletedPresent.getDeleted() == true);
			lastDeletedPresent = pMapper.getPresentByCparty(lastCparty.getId(), true).get(0);
			assertTrue(lastDeletedPresent.getId().compareTo(lastPresent.getId()) == 0);
			assertTrue(lastDeletedPresent.getDeleted() == true);
			
// PresentMapper.restorePresent			
			//restore
			pMapper.restorePresent(lastDeletedPresent.getId());
			lastDeletedPresent = pMapper.getLastPresent(false);
			assertTrue(lastDeletedPresent.getId().compareTo(lastPresent.getId()) == 0);
			assertTrue(lastDeletedPresent.getDeleted() == false);
			
// CounterpartyMapper.deletePresentFromCparty
			//DELETE
			cMapper.deletePresentFromCparty(lastPresent.getId(), lastCparty.getId());
			session.commit();
			
// PresentMapper.getPresentByCparty			
			//test
			List<Present> deletedPresent = pMapper.getPresentByCparty(lastCparty.getId(), false);
			boolean deletedMark = true;
			//find deleted present
			for(Present it : deletedPresent)
			{
				if(it.getId() == lastPresent.getId())
				{
					deletedMark = false;
					break;
				}				
			}
			assertTrue(deletedMark);				
			
			
// PresentMapper.getPresentById	
			Present dPresent = pMapper.getPresentById(lastPresent.getId());
			assertNotNull(dPresent);
			assertTrue(dPresent.getId().compareTo(lastPresent.getId()) == 0);
// PresentMapper.deletePresent				
			//delete present
			pMapper.deletePresent(lastPresent.getId());
			session.commit();
// PresentMapper.getPresentById
			dPresent = pMapper.getPresentById(lastPresent.getId());
			assertTrue(dPresent == null);
			
			//delete present type
			pMapper.deletePresentType(lastType.getId());
					
		
// CounterpartyMapper.getCounterpartyById				
			Counterparty dCparty = cMapper.getCounterpartyById(lastCparty.getId());
			assertNotNull(dCparty);
			assertTrue(dCparty.getId().compareTo(lastCparty.getId()) == 0);
// CounterpartyMapper.deleteCounterparty				
			//delete counterparty
			cMapper.deleteCounterparty(lastCparty.getId());
			session.commit();
// CounterpartyMapper.getCounterpartyById	
			dCparty = cMapper.getCounterpartyById(lastCparty.getId());
			assertTrue(dCparty == null);
		}
		finally
		{
			session.close();
		}		
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	// ORDERS
///////////////////////////////////////////////////////////////////////////////////////////////////////
	@Test
	public void postcardCRUDshouldWork()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{
			PostcardMapper mapper = session.getMapper(PostcardMapper.class);
			
			String name = "TestPostCard11";
			String image = "http://test.ru/image.png";
			
//-------- addPostcard			
			mapper.addPostcard(name, image, true);
			session.commit();
			
//-------- getPostcards
			List<Postcard> values = mapper.getPostcards(true, false);
			assertNotNull(values);
			assertTrue(values.size() > 0);
			
			Postcard pc = values.get(0);
			assertNotNull(pc);
			
			//test the added postcard
			assertTrue(pc.getName().equals(name));
			assertTrue(pc.getImage().equals(image));
			
//---------- updatePostcard
			String newImage = "1-1t";
			mapper.updatePostcard(pc.getId(), name, newImage, false);
			session.commit();
			
			values = mapper.getPostcards(false, false);
			assertNotNull(values);
			assertTrue(values.size() > 0);
			
			pc = values.get(0);
			assertNotNull(pc);	
			
			assertTrue(pc.getImage().equals(newImage));
			
			//clear
			mapper.deleteTestPostcard();
			session.commit();
		}
		finally
		{
			session.close();
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
	// ORDERS
///////////////////////////////////////////////////////////////////////////////////////////////////////
	@Test
	public void orderCRUDshouldWork()
	{
		assertNotNull(conn);
		SqlSession session = conn.getSessionFactory().openSession();
		try
		{	
			OrderMapper mapper = session.getMapper(OrderMapper.class);
			PostcardMapper mapperPsc = session.getMapper(PostcardMapper.class);
			
			//add postcards
			String postcardName = "TestPostCard11";
			mapperPsc.addPostcard(postcardName, "order1Image", true);
			mapperPsc.addPostcard(postcardName, "order2Image", true);
			mapperPsc.addPostcard(postcardName, "order3Image", true);
			session.commit();
			
			List<Postcard> postcards = mapperPsc.getPostcards(true, false);
			assertNotNull(postcards);
			assertTrue(postcards.size() > 2);
			
			
			Order or1 = new Order();
			or1.setRecipient("Name1 Recipient");
			or1.setSender("Name1 Sender");
			or1.setComment("TestTestTt11");
			or1.setMessage("TestOrder1");
			or1.setPostcardId(postcards.get(0).getId());
			or1.setDeliveryDate(MyCalendar.makeSqlDate(2000, 1, 20));
			or1.setStartTime(MyCalendar.makeSqlTime(14, 0, 0));
			or1.setFinishTime(MyCalendar.makeSqlTime(16, 0, 0));
			or1.setComplexity(5);
			or1.setPrice(999.0f);
			or1.setCity("Ставрополь");
			or1.setStreetname("Мира");
			or1.setNumber("11/1");
			or1.setRoom("123");
			or1.setPostalcode("54321");
			or1.setTdate(MyCalendar.makeSqlDate(2000, 1, 19));
			or1.setTtime(MyCalendar.makeSqlTime(11, 41, 0));
			
			Customer cs1 = new Customer();
			cs1.setFirstName("Firstname");
			cs1.setLastName("Test1One");
			cs1.setMiddleName("Middlename");
			cs1.setPhonenumber("1234567");
			cs1.setEmail("myemail@mail.com");
			
//----------THE SECOND
			Order or2 = or1.copy();
			or2.setMessage("TestOrder2");
			or2.setPostcardId(postcards.get(1).getId());
			or2.setDeliveryDate(MyCalendar.makeSqlDate(2000, 1, 25));
			or2.setStartTime(MyCalendar.makeSqlTime(18, 0, 0));
			or2.setFinishTime(MyCalendar.makeSqlTime(20, 0, 0));
			or2.setTdate(MyCalendar.makeSqlDate(2000, 1, 22));
			or2.setTtime(MyCalendar.makeSqlTime(11, 42, 0));
			
			Customer cs2 = cs1.copy();
			cs2.setLastName("Test2Two");
			cs2.setPhonenumber("2234567");
			
//----------THE THIRD
			Order or3 = or1.copy();
			or3.setMessage("TestOrder3");
			or3.setPostcardId(postcards.get(2).getId());
			or3.setDeliveryDate(MyCalendar.makeSqlDate(2000, 1, 27));
			or3.setStartTime(MyCalendar.makeSqlTime(20, 0, 0));
			or3.setFinishTime(MyCalendar.makeSqlTime(22, 0, 0));
			or3.setTdate(MyCalendar.makeSqlDate(2000, 1, 25));
			or3.setTtime(MyCalendar.makeSqlTime(11, 42, 0));
			
			Customer cs3 = cs1.copy();
			cs3.setLastName("Test3Three");
			cs3.setPhonenumber("3234567");
			
			//----add first
			mapper.addOrder(
					"", 
					or1,
					cs1
			);
			//----add second
			mapper.addOrder(
					"", 
					or2,
					cs2
			);
			//----add third
			mapper.addOrder(
					"", 
					or3,
					cs3
			);
			session.commit();
			
			//GET orders
			List<Order> orderList = mapper.getOrders(true, false);
			assertNotNull(orderList);

			for(Order it : orderList)
				System.out.println(it.getDeliveryDate());
			
			//EQUEL = 2
			assertTrue(orderList.get(0).getPostcardId().compareTo(postcards.get(0).getId()) == 0);
			//EQUEL = 1
			assertTrue(orderList.get(1).getPostcardId().compareTo(postcards.get(1).getId()) == 0);
			//EQUEL = 0
			assertTrue(orderList.get(2).getPostcardId().compareTo(postcards.get(2).getId()) == 0);
			
			//EQUEL = TestOrder1
			assertTrue(orderList.get(0).getMessage().equals("TestOrder1"));
			//EQUEL = TestOrder2
			assertTrue(orderList.get(1).getMessage().equals("TestOrder2"));
			//EQUEL = TestOrder3
			assertTrue(orderList.get(2).getMessage().equals("TestOrder3"));
			
			
			mapper.changeStatus(orderList.get(1).getId(), Order.STATUS_SHIPPED);
			session.commit();
			
			//GET orders
			orderList = mapper.getOrders(true, true);
			
			//EQUEL = TestOrder1
			assertTrue(orderList.get(0).getMessage().equals("TestOrder2"));
			//EQUEL = TestOrder3
			assertTrue(orderList.get(1).getMessage().equals("TestOrder1"));
			//EQUEL = TestOrder2
			assertTrue(orderList.get(2).getMessage().equals("TestOrder3"));
			
			//getCustomerByOrder
			Customer testCus1 = mapper.getCustomerByOrder(orderList.get(1).getId());
			assertTrue(testCus1.getLastName().equals(cs1.getLastName()));

			mapper.deleteByCommentTest();
			mapperPsc.deleteTestPostcard();
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
}
