package com.ermolaev.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class TestConnection {
	private SqlSessionFactory sessionFactory;
	
	public TestConnection()
	{
        File file = new File(getClass().getResource("/dbConfig/postgresql/config/mybatis-config.xml").getFile());
        
		try(InputStream inputStream = new FileInputStream(file))
		{
			sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public SqlSessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SqlSessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
